< Înapoi la [Cuprins](../cuprins.md) 

# Prevederi Generale #
## Utilitatea documentului
todo

## Conținutul documentului ##

Zona examinata http://api-db.chisinau.xyz/relation/60

* Complexul Rezidențial 1 - Hotar: http://api-db.chisinau.xyz/relation/57
* Complexul Rezidențial 2 - Hotar: http://api-db.chisinau.xyz/relation/58
* Complexul Rezidențial 3 - Hotar: http://api-db.chisinau.xyz/relation/55
* Complexul Rezidențial 4 - Hotar: http://api-db.chisinau.xyz/relation/56
* Complexul Rezidențial 5 - Hotar: http://api-db.chisinau.xyz/relation/45
* Complexul Rezidențial 6 - Hotar: http://api-db.chisinau.xyz/relation/38
* Complexul Rezidențial 7 - Hotar: http://api-db.chisinau.xyz/relation/37
* Complexul Rezidențial 8 - Hotar: http://api-db.chisinau.xyz/relation/40
* [Complexul Rezidențial 9](v5_CR9.md) - Hotar: http://api-db.chisinau.xyz/relation/35
* [Complexul Rezidențial 10](v5_CR10.md) - Hotar: http://api-db.chisinau.xyz/relation/36
* [Complexul Rezidențial 11](v5_CR11.md) - Hotar: http://api-db.chisinau.xyz/relation/34
* [Complexul Rezidențial 12](v5_CR12.md) - Hotar: http://api-db.chisinau.xyz/relation/41

< Înapoi la [Cuprins](../cuprins.md) 