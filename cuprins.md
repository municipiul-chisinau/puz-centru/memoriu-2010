# MEMORIU GENERAL #

## Volumul I - Memoriu general (piese scrise) ##
### [Capitolul I. Introducere](volumul1/v1_cap1.md) ###

* [1.1 Fundamentarea lucrării](volumul1/v1_cap1/v1_cap1.1.md)
* [1.2 Scopul lucrării](volumul1/v1_cap1/v1_cap1.2.md)
* [1.3 Date inițiale](volumul1/v1_cap1/v1_cap1.2.md)
* [1.4 Surse documentare](volumul1/v1_cap1/v1_cap1.2.md)

### [Capitolul II. Descriere istorică privind dezvoltarea nucleului istoric al or.Chișinău](volumul1/v1_cap2.md) ###

### [Capitolul III. Analiza situației existente](volumul1/v1_cap3.md) ###
* [3.1 Condițiile naturale](volumul1/v1_cap3/v1_cap3.1.md)
  * 3.1.1 Condițiile geomorfologice
  * 3.1.2 Condițiile geologico-inginerești
  * 3.1.3 Condițiile hidrogeologice
  * 3.1.4 Condițiile ecologice 
* [3.2 Evaluarea arhitectural-peisajistică](volumul1/v1_cap3/v1_cap3.2.md)
* [3.3 Situația urbanistică și spațial-volumetrică](volumul1/v1_cap3/v1_cap3.3.md)
* [3.4 Descriere patrimonială](volumul1/v1_cap3/v1_cap3.4.md)
* [3.5 Zonificare funcțională](volumul1/v1_cap3/v1_cap3.5.md)
* [3.6 Obiective social-culturale, administrative, științifice, de educație, învățământ, sportive și de producere](volumul1/v1_cap3/v1_cap3.6.md)
* [3.7 Situația socio-economică](volumul1/v1_cap3/v1_cap3.7.md)
* [3.8 Populația, situația demografică](volumul1/v1_cap3/v1_cap3.8.md)
* [3.9 Spațiul locativ](volumul1/v1_cap3/v1_cap3.9.md)
* [3.10 Structura stradală și de transport](volumul1/v1_cap3/v1_cap3.10.md)

### [Capitolul IV. Reglementări](volumul1/v1_cap4.md) ###
* [4.1 Reglementări urbanistice](volumul1/v1_cap4/v1_cap4.1.md)
  * 4.1.1 Structura administrativ-teritorială
  * 4.1.2 Zonificarea funcțională
  * 4.1.3 Zonificarea patrimonială
  * 4.1.4 Caracteristica fondului imobiliar 
* [4.2 Structura drumurilor și de transport](volumul1/v1_cap4/v1_cap4.2.md)
  * 4.2.1 Rețeaua de drumuri și străzi
  * 4.2.2 Linii roșii ale străzilor
  * 4.2.3 Organizarea circulației rutiere
  * 4.2.4 Accese pietonale și de biciclete
  * 4.2.5 Obiecte de deservire a transportului
  * 4.2.6 Tansportul public urban
* [4.3 Populația, situația demografică](volumul1/v1_cap4/v1_cap4.3.md)
* [4.4 Spațiul locativ](volumul1/v1_cap4/v1_cap4.4.md)
* [4.5 Dezvoltarea socio-economică](volumul1/v1_cap4/v1_cap4.5.md)
  * 4.5.1 Dezvoltarea orașului în calitate de centru administrativ, de afaceri, financiar și de transport
  * 4.5.2 Formarea atracției arhitecturale a orașului, în concordanță cu peisajul natural
  * 4.5.3 Dezvoltarea economică a capitalei
  * 4.5.4 Dezvoltarea infrastructurii sociale
  * 4.5.5 Dezvoltarea infrastructurii turismului

### Capitolul V. Utilități publice. Situația existentă, reglementări ###
* [5.1 Alimentare cu apă și canalizare](volumul1/v1_cap5.md)
  * 5.1.1 Date inițiale
* 5.2 Alimentarea cu apă
  * 5.2.1 Situația existentă
  * 5.2.2 Consumul de apă
  * 5.2.3 Schema de alimentare cu apă
  * Tabel 5.2.1
* 5.3 Canalizarea
  * 5.3.1 Situația existentă
  * 5.3.2 Evacuarea apelor uzate
  * 5.3.3 Schema de evacuare a apelor uzate
  * Tabel 5.3.1
* 5.4 Alimentare cu energie termică
  * Tabel 5.4.1
  * Tabel 5.4.2
* 5.5 Alimentarea cu gaze naturale. Situația existentă
* 5.6 Alimentarea cu gaze naturale. Reglementări
* 5.7 Alimentarea cu energie electrică
  * 5.7.1 Situația existentă
  * 5.7.2 Propuneri de proiect
  * Tabel 5.7.1
  * Tabel 5.7.2
* 5.8 Telefonizare și radioficare
  * 5.8.1 Situația existentă
  * 5.8.2 Propuneri de proiect
  * 5.8.3 Radioficare. Propuneri de proiect
  * Tabel 5.8.1

### Capitolul VI. Anexe ###
* Anexa nr.1 Indicii tehnico-economici de bază ai PUZ Centru
* Anexa nr.2 Obiectele Autoritățile puterii și administrației de stat ale RM
* Anexa nr.3 Termeni de referință privind elaborarea Planului Urbanistic Zonal Centru, or.Chișinău
* Anexa nr.4 Scrisoarea nr.79/2260 din 14.04.2011 a S.A. "Termocom" cu propuneri asupra proiectului "Elaborarea planului urbanistic zonal Centru, or.Chișinău".

## Volumul I/a - Memoriu general. Compartimentul protecția mediului ##

## Volumul II - Analiza situației existente. Generalități (piese desenate) ##

## Volumul III - Analiza situației existente. Disfuncționalități și priorități din cartiere (piese desenate) ##

## Volumul IV - Reglementări (piese desenate) ##

## [Volumul V - Regulament aferent Planului Urbanistic Zonal Centru, or.Chișinău](volumul5/v5_general.md) ##

## Volumul VI - Material ilustrativ (piese desenate) ##

## Volumul VII - Album ilustrativ ##