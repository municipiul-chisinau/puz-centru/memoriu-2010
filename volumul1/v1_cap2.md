< Înapoi la [Cuprins](../cuprins.md)

## Capitolul II. Descriere istorică privind dezvoltarea nucleului istoric al or.Chişinău ##
Orașul Chișinău a fost inființat la intersecția căilor comerciale principale ale Moldovei (de la Cetatea Albă Belgorod-Dnestrovsk și Chilia la Sculeni și de la vest la est - "drumul spre Moldova") și a fost menționat pentru prima dată pe 17 iulie 1436. Aceasta dată se consideră data fondării orașului. 

Concomitent cu or.Chișinău, pe un teritoriu restrîns (în limita razei orașului contemporan), se aflau și alte localități: Buiucani, Hrusca, Voviceni, Visterniceni, Ghеțеоаnа, Muncești, Schinoasa etc, care au apărut, ca și Chișinău în sec. XV-XVI.

Se consideră, că viața orașului a început de la izvoarele în regiunea colinei Mazarachi, unde au apărut și primele cartiere locative. La poalele colinei, în sec. XVIII a fost construit "havuzul ce scurge apă minunată prin patruzeci de orificii". Havuzul este menționat în notele lui A. Demidov, care a vizitat orașul în anul 1837.

După părerea lui, acest "frumos havuz, ce dă apa în scurgere liberă" a rămas în Chișinău de la turci.

La construcția "havuzului" au fost utilizate izvoarele existente la poalele colinei, de la care, prin galerie subterană specială, apa era livrată la cele 40 de orificii ale havuzului. Accesul convenabil permitea să fie umplute butoaiele pentru apă și livrate în întregul oraș. Havuzul principal al orașului timp îndelungat era unca sursă de alimentare cu apă.

În sec. XV-XVIII, Chișinăul ocupa o fîșie îngustă de-a lungul malului drept al rîului Bîc. În preajma colinei, pe care ulterior a fost ridicată biserica Mazarachi, erau amplasate case cu un etaj de tip rural în spatele gardurilor oarbe. Cartierele vechi aveau forme bizare și se deosebeau nu doar prin diversitate ci și prin dimensiuni. 

Toate drumurile veneau spre podul peste rîul Bîc în regiunea colinei Rîșcani. Ele au determinat trasarea străzilor principale ale orașului. 

În apropiere nemijlocită locului de trecere și havuzului se afla și prima biserica a Chișinăului - biserica sf. Nicolae (contemporana biserică armenească), care a fost distrusă de turci în anul 1739. Propunerea construirii bisericii armenești pe locul celei moldovenești e confirmată de către cercetătorii: G. Bezvekonnîi, A. H. Taramanean. În plus, drept confirmare a acestui fapt, servește planul bisericii caracteristic pentru bisericile moldovenești răspîndite în sec. XVI-XVII (biserica moldovenească din Orheiul Vechi, biserica Adormirii Maicii Domnului din Căușeni).

Pe parcursul dezvoltării, orașul ocupa terenurile ocinilor mănăstirești adiacente. Astfel, în urma distrugerii orașului de către tătari în 1739, mulți orășeni, după cum se menționează în istoria or. Chișinău, "au înălțat clădiri pe pămîntul ocinei manăstirești Buiucani". Este vorba, după părerea noastră, despre teritoriul de la vest de piața veche, despre așa numita colina Inzov, unde în a.1741, pe un teren imens cumpărat de la mănăstire, serdarul Nastasii Lupu, din cont propriu, a zidit o biserică nouă, în locul celei arse de tătari și a declarat-o apartenentă mănăstirii Galata. Se pare că e vorba despre Catedrala Veche (biserica Arhangheluli Mihail). În acel timp ea era unica biserică în Chișinău. În jurul bisericii și pe teritoriul ei s-a constituit ulterior centrul nou și s-a instaurat majoritatea orașului reconstruit. 

Aceasta permite să concluzionăm că, în perioada anilor 50 a sec. XVIII, centrul orașului s-a strămutat și s-a format în regiunea Catedralei Vechi, unde se instalau negustorii, moșierii, se afla casa guvernatorilor și ale altor persoane de vază în oraș.

Cea mai timpurie schemă de care dispunem este "Planul Chișinăului" a taberei armatelor rusești în apropierea orașului în anul 1789, după care se poate aprecia dimensiunea și caracterul construcției orașului la sf. sec. XVIII. Chișinăul, pe acel timp, ocupa un teritoriu îngust de-a lungul rîului Bîc, cu lățimea 400-500 m și lungimea 1500 m. 

În regiunea străzilor Anton Pann și A. Botezatu, se intersectau într-un singur nod drumurile din 3 direcții: din suburbia Buiucani, Hîncești și Ismail, Iași. Din nord-est (în locul intersectării Strada Ismail de azi cu r.Bîc) spre oraș, venea drumul din partea Tighinei și Căușenilor, ce trece apoi în Strada Balișevskaia (azi StradaAlbișoara). Drumul de est lega orașul cu satul Muncești (StradaGr.Ureche). Din nord-vest venea drumul Hotinului (Calea Orheiului). Toate drumurile se interesectau lîngă podul peste r. Bîc în regiunea colinei Rîșcani.

Dispunem și de "planul vechi al localității în locul orașului Chișinău cu indicarea ocinelor mănăstirești", prezentat într-un dosar, în anul 1800, în Divanul Moldovenesc.

În scheme sunt evidențiate foarte clar străzile dens construite ale nucleului orașului: Fontannaia, Armenească, Cahul, Turcească, organizarea accesului la piața veche. (Denumirea străzilor e dată în conformitate cu planul lui Fiodorov din anul 1834). În centrul orașului este reprezentat r.Bîc intersectat de baraje, pe ambele maluri ale căruia sunt împrăștiate rarele casuțe. 

Schemele oferă o imagine certă despre amplasarea clădirilor mai importante ale orașului din sec.XVIII: biserica sf. Constantin și Elena, casa gospodărească pe colina Rîșcani, casa lui Donici, Catedrala Veche, biserica Adormirii Maicii Domnului. 

Pantele abrupte ale văii rîului Bîc din partea de sud și de vest, teren puternic accidentat din cea de est au contribuit la formarea în a.1789 în regiunea bisericii sf.Constantin și Elena din Rîșcani a construcțiilor de apărare din nord a taberei armatei ruse din Chișinău. 

Teritoriul în jurul bisericii cu lungimea de 200 stînjeni și lățimea de 100 a fost întarită cu un dig și un șanț (procedeu tipic pentru fortificările hotarelor de vest ale Rusiei la înc. sec.XVIII) cu 3 bastioane și un bastion aparte la nord de cetate. 

Conform schemelor numite, se poate aprecia dimensiunea și caracterul construcției doar a Chișinăului și a Visternicenilor la sf. sec. XVIII.

Planul orașului și amplasarea în el a celor mai importante clădiri au fost rezultat al utilizării bine gândite a situației topografice. Toate bisericile și clădirile importante ocupau terenurile deasupra văii r.Bîc.

Structura orașului se diviza în celule-cartiere de forme neregulate bine vizibile. Drumurile din 5-6 direcții intersectîndu-se într-un punct, formau pieți de forme plane neregulate ce serveau drept trecere.

Piețile unde se intersectau drumurile din suburbii se numeau "praștii" (Muncești, Sculeni, Tighina, Orhei).

Drept bază a sistematizării construcției au servit terenurile agrare a căror dimensiuni și proporții depindeau de rangul proprietarului.

Însă chiar și în cazul planificării atît de pitorești (haotice), spațiul orașului (inclusiv Vistrerniceni) era evident sistematizat prin construcții vizual-perceptibile. Clădirile cele mai înalte - bisericile vechi ale orașului: Catedrala Veche, biserica sf. Constantin și Elena de la Rîșcani, biserica Bunei Vestiri, biserica Mazarachi, pe planul orașului formează un romb cu latura de cca. 600 m (distanță de vizibilitate bună).

Bisericile apărute mai tîrziu sunt amplasate considerînd sistemele existente, între biserici vechi și noi de asemenea se pastrează distanța de 500-600 m.

În planificarea Chișinăului vechi se utiliza pe larg principiul compozițional de fixare a direcțiilor noi prin clădiri înalte. Spre Catedrala Veche au fost orientate drumurile de bază ce veneau spre oraș (direcțiile Hîncești, Iași, Tighina).

La începutul sec. XIX, biserica Bunei Vestiri încheia perspectiva intrării din Orhei, biserica sf. Gheorghe - fixa direcția Tighina, sf. Ilie era percepută de pe drumul Buiucani, drumul Reni era orientat spre biserica Tuturor Sfinților.

"Străzile vechi, înguste s-au umplut de căsuți mici ca de ciuperci. Casele se înghesuiesc una lîngă alta parcă se susțin reciproc și depind una de alta."

"...În schimb celelalte strazi sunt numai garduri, fiindcă aproape toate casele sunt construite în mijlocul ogrăzilor cu livezi, grădini, gospodării - exact moșiile sătești în miniatură. Din cînd în cînd apare vreo inovație, casa cu grădinița în afară - casele demnitarilor și comercianzilor ruși. Nici pavaj, nici trotuar, nici felinare, nici scările nu se văd.

...Numai în partea turcească a orașului (mahala turcească), unde locuiesc și bulgarii, domnește liniște absolută și mocneală. Străzile aici sunt cu stradele nenumărate, iar locuințele seamănă cu închisori - cu gratii pe ferestre chiar sub acoperiș." (Olga Nakko "Din viața lui A.S. Pușkin în Chișinău." anul 1899, St. Petersburg).

Către a.1850 suprafața generală a terenurilor orașului a atins 4670 ha, inclusiv sub cladirile urbane se aflau 758 ha.
În condițiile jugului turcesc, instaurat în Chișinău în mijlocul sec.XVI, invaziilor permanente a turcilor și a tătarilor, dezvoltarea orașului era lentă. 

Numărul locuitorilor din anul 1774 pînă în anul 1803 s-a mărit de la 600 locuitori la 1400 locuitori. Locuitorii, pe lîngă comerț și meșteșug se ocupau cu agricultura. 

Dezvoltarea Chișinăului drept localitate urbană se începe numai odată cu eliberarea Basarabiei de sub jugul otoman în a.1787. Ritmul creșterii sale a accelerat și mai mult începînd cu anul 1812, cînd Basarabia s-a unit cu Rusia. În a.1818 Chișinăul devine centrul administrativ și cultural al Basarabiei. 

De atunci a inceput construirea planificată a orașului, precedat de efectuarea ridicării topografice din a.1813 de către agrimensorul Ozmidov și inginerul-arhitect Harting. În a.1814 în oraș se numărau 2109 case, 7 biserici și o mănăstire (Armenească).

Conform ridicării topografice noi din anii 1829-1830, generalul-maior P.I.Fiodorov a constituit un proiect de sistematizare a construcției existente a orașului și construcției terenurilor libere din partea de sus - primul plan general al orașului acceptat de țarul Nicolae în a.1834.

O particularitate importantă a acestui plan este faptul că tot orașul vechi, pînă la StradaMitropolit Dosoftei, este reprezentat fără schimbări, cu stradelele sale strîmbe, iar apoi, deasupra linillor acestea strîmbe, a grupurilor neregulate de construcții ale orașului vechi, sunt trasate cartierele rectilinii ale orașului nou conform proiectului lui Bahmetiev completat de Fiodorov. Pe planul lui Fiodorov sunt date pentru prima dată denumirile străzilor. Cartierele planificate din nou și cele neconstruite sunt indicate cu linie întreruptă.

Descrierile mărturisesc faptul că orașul în această perioadă a început să se dezvolte activ înspre vest de la StradaCăușeni (azi Strada Alexandru cel Bun) și construcția orașului de sus a devenit regulată. Orașul vechi nu și-a schimbat aspectul pînă în a.1834, cînd de soarta lui s-a ocupat guvernatorul energic generalul Pavel Ivanovici Fiodorov. Pe lîngă popularea orașului de sus, în raza celui vechi de asemenea se petreceau schimbări esențiale. Cartierele vechi se încadrau în formele rectangulare noi. Aceasta nu era ușor, deoarece cartierele vechi, în plan, după cum afirma Ivan Halippa, erau "de forme minunat năstrușnice".

În rezultatul măsurilor decisive întreprinse de către Fiodorov a fost complet schimbată structura de planificare a orașului vechi. 

Teritoriul mai sus de StradaAlexandru cel Bun a fost împărțit în cartiere dreptunghiulare cu dimensiuni 60-80 m pe 100-120 m, ce se construiau, de regulă, perimetric. Astfel a apărut așa numitul orașul "nou" de sus, iar cel de jos a fost numit "vechi". Ca bază pentru planificarea orașului "nou" a servit direcția trecerilor taberei armatelor ruse, existente pe teritoriu în a.1789. Către începutul sec. XIX în regiunea străzii Alexandrovskaia (azi Bulevardul Ștefan cel Mare), care era considerată drept axă planimentrică principală a orașului, s-a reamplasat și centrul orașului, unde ulterior au fost concentrate principalele clădiri și pieți orășenești.

Element principal al structurii de planificare a orașului a devenit Piața Catedralei. Axa transversală a pieții o constituie clădirile complexului: Catedrala, clopotnița, arca de triumf. Catedrala ocupă perspectiva cîtorva străzi din rețea: Strada Pavlov (azi Strada Petru Rareș) și StradaGostinnaia (azi Strada Mitropolit Dosoftei).

În orașul nou, piețile au devenit unul din elementele compoziționale principale. Ele se încadrau în rețeaua dreptunghiulară a străzilor, dimensiunile maxime a unora din ele atingînd 300m. 

În prima jumătate a sec. XIX în oraș se numărau 15 pieți: a catedralei, Ciuflea, piața tîrgului vechi, bazarul ș.a.

Construcțiile orașului preponderent aveau un etaj (85%). Spre sf. sec. XIX în oraș existau doar 4 cladiri cu 3 etaje (gara feroviară, casa Eparhiei, castelul închisorii, casa lui Schwartzman). Imaginea orașului o determinau bisericile și catedralele. La începutul sec.XX în oraș se numărau cca. 20 construcții de cult.

Orașul nou se dezvolta independent, și din punct de vedere planimetric, nu avea tangențe cu orașul vechi. Prima încercare de a le uni a fost întreprinsă de guvernatorul Fiodorov în anii 1840, cînd cartierele vechi se reduceau, iar străzile erau încadrate în noile forme rectangulare.

Valea r.Bîc spre sfîrșitul sec. XIX a început să se valorifice pentru amplasarea căii ferate, întreprinderilor industriale, depozitelor. Orașul s-a redirecționat de la spațiu deschis, care și-a pierdut principala sa valoare compozițională. 

În a.1834 Chișinăul ocupa suprafața de 4733,2 ha, sub cladiri urbane se aflau 718,3 ha. Orașul era intersectat în direcții diferite de 90 străzi și stradele largi și drepte. În a.1812 în oraș locuiau 7 mii locuitori, în a.1844 - 52 mii, în 1861 - 93 mii, în a.1919 - 133 mii locuitori.

În prima jum. a sec.XIX, economia Chișinăului rămînea semiagrară. Locuitorii se ocupau, preponderent, cu pomicultura, legumicultura și viticultura. În a.1860 în oraș erau 122 mori, inclusiv doua cu aburi, 8 uzine de alcool și rachiu, mici uzine de producere a cărămidei, de vopsele, textile și alte întreprinderi. La sf.sec. XIX - înc. sec. XX principalele ramuri ale industriei se bazau pe prelucrarea produselor agricole. În oraș activau distilerii, leviatane, atelier de prelucrare a tutunului. Existau întreprinderi de producere și reparație a uneltelor agricole, gatere, uzină de lumînări și altele. 

Odată cu acestea, în oraș continua să se dezvolte meșteșugăritul (producerea încălțămintei, a hainelor, prelucrarea lemnului, a metalelor, producția ceramică - în total mai mult de 100 ramuri). În a. 1897 în Chișinău locuiau cca. 10 mii de meșteșugari.

Către acel moment, suburbii ale Chișinăului au devenit satele Buiucani, Schinoasa și Malina, Tabacaria, Rîșcani, Visterniceni, Muncești. În a.1910 în Chișinău se numărau 10 mii de case, 142 străzi și stradele, 12 pieți, 5 livezi și parcuri.

Începînd cu a.1888 cel mai popular transport public a devenit tramvaiul cu tracțiune de cai (конка), doar din a.1914 a fost deschisă circulația tramvaiului, punctele terminus ale carora se aflau în regiunile "praștiilor" (Hîncești, Muncești, Sculeni) și stația Visterniceni. În a.1871 au fost construite principalele cladiri ale gării feroviare. 

Serviciile culturale și medicale se dezvoltau în felul următor:

* În a. 1813 a fost deschis seminarul teologic din Chișinău - unica instituție de învățămînt în oraș în acel timp; în a.1833 - gimnaziul regional. În a.1813 a fost fondată tipografia, în a.1832 - biblioteca publică.
* În a.1817 a început să funcționeze primul spital cu 70 de paturi, iar în a.1828 - spital cu 310 paturi. Apoi au fost construite spitalul pentru copii și spitalul infecțios și alte instituții medicale, dar evident, se simțea insuficiența acestora, în plus serviciile medicale erau contra plată și inaccesibile pentru toți. În a.1918-40 la 10 mii louitori reveneau 0,4 medici și 0,6 lucrători medicali medii.

În a.1918-40 a diminuat producția industrială, un șir de întreprinderi mari au fost reîntemeiate în România, a fost exclusă industria pielii, de prelucrare a metalelor și alte ramuri industriale. Chișinăul se transforma în orașul atelierelor meșteșugărești și semi-meșteșugărești. Populația orașului în a.1939 a diminuat pînă la 109 mii locuitori.

Construcții noi nu se efectuau, cu mici excepții, ceea ce a contribuit la diferențierea puternică între orașul de sus cu vile și instituții municipale și periferia săracă și neamenajată.

În timpul ocupării fasciste, au fost distruse practic toate întreprinderile industriale, 9388 case de locuit (cca. 76% din fondul locativ), clădiri de educație și administrative, telefonul, telegraful. 

Spre începutul a.1946 au fost reconstruite 75 întreprinderi industriale, apeductul, 7 policlinici ș.a. Populația a primit 93 mii m2 de spațiu de locuit restabilit. Elaborarea noului plan de reconstrucție a Chișinăului s-a realizat sub conducerea academicianului A.V.Șciusev.

Conform planului de reconstrucție, în cartierele vechi distruse au fost inițiate Bulevardul Renașterii, Bulevardul Negruzzi, se construiau străzile deja concepute și magistralele (Bulevardul Ștefan cel Mare, Strada 31 august 1989, Strada Pușkin, Strada Banulescu-Bodoni, Strada Armenească, Strada Vlaicu-Pîrcalab, Strada Tighina etc.), au fost organizate cele mai semnificative pieți ale orașului (Marii Adunări Naționale, Libertății, Bulevardul C.Negruzzi, D.Cantemir, Gării, P.Halippa).

Academicianul A.V. Sciusev la întocmirea planului de reconstrucție a orașului, a întreprins în anii 1945-1947 încă o încercare de a include în compoziția orașului spațiul r.Bîc și unirea planimetrică a orașului de sus cu cel de jos. În acest scop, orașul vechi trebuia să fie străpuns de căteva magistrale. 
Trei raze (Strada Pușkin, Bulevardul Renașterii și Strada Petru Rareș) se desfăceau de la piața catedralei în direcția r. Bîc cu scopul evidențierii amplasării și asigurarea direcției de dezvoltare a centrului municipal comun și deschiderea sa spre spațiul văii rîului. În acest caz văii r.Bîc i se acorda semnificație principală în structura de planificare a orașului.

Pe lîngă inițierea Bulevardul Renașterii, se reconstruiesc și alte noduri urbanistice. De la sf. anilor 1946 se concepe o noua scară de construcție a Bulevardul Ștefan cel 

Mare, se formează compoziția piețelor Marii Adunări Naționale, Libertății, Gării.

În locul "praștiilor" la intrări în partea centrală a orașului a început construcția piețelor D.Cantemir, P.Halippa, au fost fondate bulevardele Gagarin și Negruzzi.

Structura de planificare a orașului vechi la efectuarea lucrărilor de reconstrucție a cunoscut schimbări esențiale: au apărut spații largi a străzilor noi, au dispărut piețile și cartierele vechi, demolate unele clădiri istorice importante.

Atenția majoră a fost acordată creării centrului public al orașului, care se dezvolta pe doua axe reciproc-perpendiculare: Bulevardul Ștefan cel Mare și Bulevardul Renașterii. 

În componența centrului se formează zonele instituțiilor de importanță republicană și municipală, instituțiilor cultural-distractive, întreprinderi de comerț, sportive și locative.

< Înapoi la [Cuprins](../cuprins.md)