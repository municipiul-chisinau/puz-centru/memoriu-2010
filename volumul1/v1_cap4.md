< Înapoi la [Cuprins](../cuprins.md)

## Capitolul IV. Reglementări ##
* [4.1 Reglementări urbanistice](v1_cap4/v1_cap4.1.md)
  * 4.1.1 Structura administrativ-teritorială
  * 4.1.2 Zonificarea funcțională
  * 4.1.3 Zonificarea patrimonială
  * 4.1.4 Caracteristica fondului imobiliar 
* [4.2 Structura drumurilor și de transport](v1_cap4/v1_cap4.2.md)
  * 4.2.1 Rețeaua de drumuri și străzi
  * 4.2.2 Linii roșii ale străzilor
  * 4.2.3 Organizarea circulației rutiere
  * 4.2.4 Accese pietonale și de biciclete
  * 4.2.5 Obiecte de deservire a transportului
  * 4.2.6 Tansportul public urban
* [4.3 Populația, situația demografică](v1_cap4/v1_cap4.3.md)
* [4.4 Spațiul locativ](v1_cap4/v1_cap4.4.md)
* [4.5 Dezvoltarea socio-economică](v1_cap4/v1_cap4.5.md)
  * 4.5.1 Dezvoltarea orașului în calitate de centru administrativ, de afaceri, financiar și de transport
  * 4.5.2 Formarea atracției arhitecturale a orașului, în concordanță cu peisajul natural
  * 4.5.3 Dezvoltarea economică a capitalei
  * 4.5.4 Dezvoltarea infrastructurii sociale
  * 4.5.5 Dezvoltarea infrastructurii turismului

< Înapoi la [Cuprins](../cuprins.md)