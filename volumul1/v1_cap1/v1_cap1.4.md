< Înapoi la [Cuprins](../../cuprins.md) | [Volumul I - Memoriu general](../v1_cap1.md)

## 1.4. Surse documentare ##

* NCM B.01.02-2005. Instrucțiuni privind conținutul, principiile metodologice de elaborare, avizare și aprobare a documentației de urbanism și amenajare a teritoriului;
* Legea privind principiile urbanismului și amenajării teritoriului nr.835-XIII din 17.05.1996;
* Legea privind autorizarea executării lucrărilor de construcție nr.163 din 09.07.2010;
* Legea privind ocrotirea monumentelor nr.1530 din 22.06.1993;
* Legea culturii nr.1093-II din 28.07.1999;
* Legea privind ariile naturale protejate de stat nr.1538-XIII din 25.02.1998;
* Regulamentul general de urbanism (HGRM nr.5 din 05.01.1998);
* Regulamentul privind zonele protejate naturale și construite (HGRM nr.1009 din 05.10.2000);
* Planul Urbanistic General (PUG) al or.Chișinău aprobat prin decizia Consiliului municipal Chișinău nr.68/1-1 din 22.03.2007;
* Planul de Amenajare a Teritoriului (PAT) municipiului Chișinău aprobat prin decizia Consiliului municipal Chișinău nr.68/1-2 din 22.03.2007;
* Anuarul statistic al Republicii Moldova 2009;
* Chișinău în cifre. Anuar statistic 2009.

< Înapoi la [Cuprins](../../cuprins.md) | [Volumul I - Memoriu general](../v1_cap1.md)