< Înapoi la [Cuprins](../../cuprins.md) | [Volumul I - Memoriu general](../v1_cap1.md)

## 1.1. Fundamentarea lucrării ##

Planul Urbanistic Zonal Centru, or.Chișinău este elaborat în conformitate cu planul de activitate și resursele financiare prevăzute pentru perioada 2008-2010, aprobate prin decizia Consiliului municipal Chișinău nr.5/1 din 20.03.2008.

Zona examinată este amplasată în centrul orașului mărginită de străzile 
[Strada Alexei Mateevici](http://api-db.chisinau.xyz/relation/4), 
[Strada Mihai Viteazul](http://api-db.chisinau.xyz/relation/5), 
[Strada Albișoara](http://api-db.chisinau.xyz/relation/9), 
[Strada Ismail](http://api-db.chisinau.xyz/relation/10), 
[Bulevardul Ștefan cel Mare și Sfînt](http://api-db.chisinau.xyz/relation/8), 
[Strada Ciuflea](http://api-db.chisinau.xyz/relation/13), 
[Strada Lev Tolstoi](http://api-db.chisinau.xyz/relation/15), 
[Strada Pantelimon Halippa](http://api-db.chisinau.xyz/relation/9) 
și constituie o suprafață de 619 ha. Distanța maximă Nord-Sud - 2360 m, distanța maximă Est-Vest - 3900 m.

Obiective strategice însoțite de politici, programe și proiecte prevăzute de Planul Urbanistic General (PUG) al [or.Chișinău](http://api-db.chisinau.xyz/relation/16) și Planul de Amenajare a Teritoriului (PAT) municipiului Chișinău aprobate prin deciziile Consiliului municipal Chișinău nr.68/1-1 și nr.68/1-2 din 22.03.2007, care au stat la baza elaborării PUZ Centru:

* Obiectiv 5 (PAT): Acordarea unei atenții speciale în găsirea formelor de valorificare, protejare și promovare a specificității patrimoniului cultural, construit al municipiului Chișinău, în peisajul cultural european.
* Politici sectoriale și teritoriale care se coordonează cu obiectivul major (obiectivul  strategic de dezvoltare 3) în dezvoltarea sistemului policentric urban și a relaților urbane rurale prevăzute în PAT.
* Obiectiv 6 (PUG): Determinarea formelor de valorificare, protejare și promovare a specificității patrimoniului cultural, construit al municipiului Chișinău, în peisajul cultural european.
* Obiectiv 5 (PUG): Diminuarea, stoparea ritmurilor de creștere a poluării, reabilitarea ecologică a zonelor naturale urbane.
* Politica 01-3 (PUG): Optimizarea și modernizarea rețelei stradale.
* Politica 04-1 (PUG): Dezvoltarea economică durabilă în plan teritorial.

< Înapoi la [Cuprins](../../cuprins.md) | [Volumul I - Memoriu general](../v1_cap1.md)