< Înapoi la [Cuprins](../../cuprins.md) | [Volumul I - Memoriu general](../v1_cap1.md)

## 1.2. Scopul lucrării ##

* Stabilirea și delimitarea zonelor în care se prevede desfășurarea operațiunilor urbanistice majore;
* Stabilirea zonelor protejate de patrimoniu;
* Delimitarea zonelor funcționale a teritoriului examinat;
* Stabilirea și delimitarea zonelor cu interdicții temporare sau definitive de construire;
* Stabilirea regimului de construire, cuprinzând:
  * aliniamentele;
  * regimul de înălțime;
  * indici de control;
  * procentul de ocupare a teritoriului (POT) și coeficientul de utilizare a terenului (CUT);
* Stabilirea reglementărilor la aspectul exterior al construcțiilor;  
* Determinarea soluțiilor de organizare și sistematizare a circulației de transport;
* Determinarea soluțiilor de organizare a infrastructurii tehnico-edilitare;
* Determinarea soluțiilor de organizare a structurii de drumuri și noduri;
* Delimitarea zonelor în care se preconizează executarea lucrărilor de utilitate publică;

< Înapoi la [Cuprins](../../cuprins.md) | [Volumul I - Memoriu general](../v1_cap1.md)