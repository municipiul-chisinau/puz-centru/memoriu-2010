< Înapoi la [Cuprins](../../cuprins.md) | [Volumul I - Memoriu general](../v1_cap1.md)

## 1.3. Date inițiale ##

* Termeni de referință privind elaborarea Planului Urbanistic Zonal Centru, or.Chișinău, oferite de Direcția generală arhitectură, urbanism și relații funciare a Primăriei  mun.Chișinău;
* suportul topografic sc 1:30 000, 1:10 000, 1:5000, 1:2000 și 1:500;
* datele fotofixării și inspecția vizuală a teritoriului efectuate de elaborator; 
* baza de date a SITE-ului "Centrul Istoric al Chișinăului" patrimoniul arhitectural al capitalei [www.monument.sit.md](http://www.monument.sit.md/);
* harta on-line a mun.Chișinău în cadrul proiectului "citymap.md" al Institutului "INGEOCAD" [ingeocad.md](http://ingeocad.md/ro/);
* datele statistice ale Departamentului de Statistică și Direcțiilor din cadrul Primăriei mun.Chișinău.

< Înapoi la [Cuprins](../../cuprins.md) | [Volumul I - Memoriu general](../v1_cap1.md)