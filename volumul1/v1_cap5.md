< Înapoi la [Cuprins](../cuprins.md)

## Capitolul V. Utilități publice. Situația existentă, reglementări ##
### 5.1 Alimentarea cu apă și canalizare ###
#### 5.1.1 Date inițiale ####
În calitate de date inițiale pentru elaborarea compatrimentului dat, servește informația referitoare la construcțiile existente, propuse și ob.1219 "Schema de alimentare cu apă și canalizare", elaborată de IMP "Chișinăuproiect".

### 5.2 Alimentarea cu apă ###
#### 5.2.1 Situația existentă ####
Raionul examinat corespunde zonei I,II a schemei de alimentare cu apă. Rețelele de apeduct stradale, cu excepția rețelelor supuse reabilitării străzilor Izmail, Pan Halipa, Ciuflea, Alecsandri, Pușkin, Bănulescu-Bodoni incluse în proiectul finanțat de BERD "Reabilitarea rețelelor de apeduct a mun. Chișinău", exploatate mai mult de 25 de ani și au o uzură considerabilă.

Rețiaua de distribuție existentă este inelară, cu diametre cuprinse între 100 și 800 mm, inclusiv și aducțiunea cu ∅1000 mm din str. Ismail.
#### 5.2.2 Consumul de apă ####
Consumatori de apă în obiectivul dat, sunt blocurile locative existente și propuse, obiective de menire socială, blocuri administrative. Consumul normativ menajer de apă potabilă este apreciat în baza CHиП 2.04.02-84 și reprezintă:

* Pentru blocuri locative dotate cu sisteme interioare de apă și canalizare, căzi de baie și încălzitoare de apă autonome, care reprezintă blocuri în unul sau două nivele, existente sau proiectate, incusiv 10% din apartamentele blocurilor cu multe nivele cu sursă de apă caldă centralizată. Această categorie corespunde normei de 160 l/om diurnă.

* Pentru blocuri locative dotate cu sisteme interioare de apă și canalizare, cu sursă de apă caldă centralizată. Această categorie corespunde normei de 230 l/om diurnă.

Debitul consumului de apă este prezentat în tabelul 5.2.1.
Debitul de apă pentru stingerea exterioară a incendiului reprezintă 2x35 l/s, unde 2 reprezintă numărul incendiilor în același timp. Debitul de apă pentru stingerea incendiului interior reprezintă 2x5l/s.

#### 5.2.3 Schema de alimentare cu apă ####
Zonificarea sistemului existent de alimentare cu apă este executat in conformitate cu devierile de relief a orașului și reprezintă baza sarcinii de execuție.
Obiectivele existente și cele propuse din partea centrală a orașului se află pe suprafețe de devieri a reliefului de la cotele 40-105m. Raionul  limitat în aval de str.Albișoara și în amonte de bd. D. Cantemir, unde relieful deviază între cotele 40-65m, se referă la I zonă de alimentare cu apă și se aprovizionează cu apă de la rezervuarele de presiune de la CET-1 și cele de la Univesitatea Agrară. Din rezervuarele de presiune apă gravitațional prin aducțiuni alimentează zona I. Presiunea piezometrică în această zonă are valoarea de 105m. Raionul limitat din partea de aval a bd. D. Cantemir și amonte străzii A. Mateevici unde relieful deviază între cotele 65-105m, se referă la zona a II de alimentare cu apă și se aprovizionează cu consum de apă și presiune de la stația de tratare. Presiunea piezometrică în această zonă are valoarea 135m. Rețelele magistrale și de distribuție cu excepția celor reconstruite enumerate mai sus, necesită reabilitare. Se preconizează construcția aducțiunilor magistrale noi pe sectorul proiectat a bd-lui D.Cantemir.

_Volumul construcțiilor noi reprezintă:_
* Rețele de apeduct ∅200 - 3,1km
* Rețele de apeduct ∅300 - 1 km
* Rețele de apeduct ∅400 - 3,2 km

_Volumul lucrărilor de reconstrucție reprezintă:_
* Rețele de apeduct ∅100 - 0,6km
* Rețele de apeduct ∅150 - 3,8km
* Rețele de apeduct ∅200 - 5,8 km
* Rețele de apeduct ∅250 - 7,0 km
* Rețele de apeduct ∅300 - 8,5km
* Rețele de apeduct ∅400 - 4,0km
* Rețele de apeduct ∅500 - 5,3km
* Rețele de apeduct ∅600 - 1,2km
* Rețele de apeduct ∅800 - 2,4km
* Rețele de apeduct ∅1000 - 1.7 km

Tabelul 5.2.1

### 5.3 Canalizarea ###
#### 5.3.1 Situația existentă ####
Partea centrală a orașului dispune de o rețea de canalizare dirvoltată. În principiu, rețelele date de canalizare sunt construite de mult timp în urmă (cu termen de exploatare mai mult de 25 ani), cu excepția celor reabilitate de pe străzile Ismail, Pan Halipa, Ciuflea, Alecsandri, Banulescu-Bodoni.

#### 5.3.2 Evacuarea apelor uzate ####
Obectivele de la care se evacuează apele uzate sunt cele existente și cele propuse: blocuri locative, obiective de menire social-culturale, blocuri administrative.

Normele de deversare a consumurilor menajere reprezintă normele consumului de apă potabilă.

Consumul de evacuare a apelor uzate este prezentat în tabelul 5.3.1.

#### 5.3.3 Schema de evacuare a apelor uzate ####
Rețelele magistrale de canalizare sunt trasate pe străzile M.Viteazul, T. Ciorbă, P. Movilă, S. Lazo, Sfatul Țării, M.Cibotaru, Bănulescu-Bodoni, A.Pușkin, V.Alecsandri, M.Eminescu, Armenească, Bulgară, Tighina, Ismail, L.Tolstoi, care gravitațional se deversează în colectorul principal de pe malul drept, care este necesar de reabilitat. Tronsoanele rețelelor de canalizare necesită reabilitare, cu excepția tronsoanelor de canalizare magistrale reconstruite și enumerate mai sus. Se propune construcția noilor colectoare pe str. Zaikin, tronsonul proiectat din str.Sfatul Țării, bd. D. Cantemir.

_Volumul construcțiilor noi reprezintă:_
* Rețele de canalizare ∅200 - 1,0 km
* Rețele de canalizare ∅300 - 3,1 km
* Rețele de canalizare ∅400 - 0,9 km

_Volumul lucrărilor de reconstrucție reprezintă:_
* Rețele de canalizare ∅200 - 2,9 km
* Rețele de canalizare ∅250 - 2,8 km
* Rețele de canalizare ∅300 - 7,9 km
* Rețele de canalizare ∅400 - 5,7 km
* Rețele de canalizare ∅500 - 3,0 km
* Rețele de canalizare ∅700 - 0,9 km
* Rețele de canalizare ∅800 - 0,2 km
* Rețele de canalizare ∅1000 - 0,5 km
* Rețele de canalizare ∅1200 - 0,7 km
* Rețele de canalizare ∅2000 - 1,3 km

Tabelul 5.3.1 - Calculul evacuării apelor uzate

### 5.4 Alimentare cu energie termică ###
Alimentarea cu căldură a centrului orașului Chișinău în timpul de față se efectuează de la rețelele termice de distribuție CET-1. Consumatori de căldură ai centrului orașului sunt blocurile locative cu 1-16 nivele, grădinițele, școlile, obiectele administrative și de utilitate publică, centrele comerciale multifuncționale. În calitate de date inițiale pentru calculul sarcinilor termice este primită dinamica schimbării fondului locativ al centrului orașului Chișinău în baza planului general. Sarcinile termice a sectorului comunal-locativ al centrului orașului pentru perioada de dezvoltare 2010-2025 se determina după indici majorați în conformitate cu СНиП 2.04.07-86 "Тепловые Сети". Pentru situația existentă, pe perioada anului 2010, sarcinile termice sunt primite conform datelor S.A. "Termocom".

Conform NCM E 01.01-2005 condițiile climaterice se caracterizează după următoarele date:

* temperatura de calcul pentru proiectarea sistemelor de încălzire și ventilare - minus 16°C;
* temperatura medie a celei mai reci luni a anului - minus 3,5°C;
* temperatura medie a sezonului de încălzire - plus 0,6°C;
* durata perioadei de încălzire - 116 zile.

Rețelele termice de la CET-1 funcționează după o schemă închisă cu 2 conducte în regim de regulare calitativă a livrării de căldură după graficul de T=130-70°C.

Partea centrală a orașului este divizată în 12 sectoare. Datele despre sarcinile termice pe sectoare separate sunt indicate în tabelul 5.4.2. Sarcinile termice a centrului orașului alcătuiesc:
* situația existentă pe perioada anului 2010 - 253 MW.
* Perioada de calcul 2010-2025 - 301,77 MW. Rețelele termice magistrale existente ale centrului orașului vor asigura pe deplin alimentarea cu căldură a acestei părți a orașului pe perioada de dezvoltare pană în anul 2025.

În legătură cu prelungirea străzii Sfatul Țării până la strada Albișoara și cu scopul îmbunătățirii fiabilității alimentării cu căldură se propune construcția unei magistrale termice noi 2Ø250 pe strada Sfatul Țării. Traseul termic urmează să fie montat prin metoda fără canale din conducte preizolate cu poliuretan expandat. În legătura cu prelungirea bulevardului Cantemir de la strada Ismail până la strada Mihai Viteazul, este necesar să fie reconstruite rețelele termice existente care sunt în zona de construcție a drumului:
* o porțiune a rețelei termice 2Ø600 de pe strada V. Alecsandri la intersecția cu bulevardul Cantemir să fie pozată în tunel de trecere cu dimensiunea de 1,8x2,4h;
* o porțiune a rețelei termice 2Ø500 de pe strada Cosmonauților să fie pozată în tunel de trecere cu dimensiunea de 1,8x2,4h;
* o porțiune a rețelei termice 2Ø600 de pe strada Mihai Viteazul urmează a fi pozate în tunel de trecere cu dimensiunea de 1,8x2,4h;
* o porțiune a rețelei termice 2Ø150 la intersecția bulevardului Catemir cu str.G. Cojbuc și str. G. Bănulescu-Bodoni să fie pozată în tunel de trecere cu dimensiunea de 1,5x2,1h;

Conectarea consumatorilor de căldură, în zona locativă, la rețelele termice exterioare se va furniza prin 2 scheme a rețelei prin PTI (puncte termice individuale).

Tabelul 5.4.1 - Indici tehnico-economici

Tabelul 5.4.2

### 5.5 Alimentarea cu gaze naturale. Situația existentă ###
Alimentarea cu gaze naturale, in prezent, a centrului orasului Chisinau are loc prin intermediul:

1. Statii reglare presiune gaz existente SRG - 7 buc.:
  * SRG nr. 7 - str. Alexei Sciusev,85;
  * SRG nr. 8 - str. Mihai Cogalniceanu, 8;
  * SRG nr. 9 - str. Anestiade, 32;
  * SRG nr. 10 - str. Gheorghe Ureche, 58;
  * SRG nr. 11 - str. Toma Ciorba, 9;
  * SRG nr. 12 - str. Alexandru cel Bun, 144/2;
  * SRG nr. 13 - str. Moara Rosie, 1.

2. Gazoducte presiune medie:
  * gazoduct presiune medie ∅500 pozat din str.Tudor Vladimirescu - str. Calea Orheiului - str.Petru Rares - str.Banulescu Bodonu - str.Alexandru cel Bun - str.Serghei Lazo - str. Mitropolit Dosoftei catre str.Calea Iesilor sectorul Sculeni;
  * gazoduct presiune medie ∅500 - ∅400 pozat din str.Trandafirilor - str.Melestiu - str.Ciuflea - str.Bucuresti - str.Mitropolit Petru Movila catre gazoductul de presiune medie ∅500 pozat la intersectia str. Mitropolit Dosoftei cu str. Mitropolit Petru Movila;
  * gazoduct presiune medie ∅200 pozat de la intersectia str.Petru Rares cu str.Ierusalim pe str.Ierusalim - str.Alexandru Hajdeu - str.Titu Maiorescu - str.Cojocarilor - str.Alexandru cel Bun - str.Tighina - str. Mitropolit Varlaam - str. Anestiade catre SRG № 9;
  * gazoduct presiune medie ∅325 pozat de la intersectia str.Bucuresti cu str.Mihai Eminescu pe str.Mihai Eminescu - str.Universitatii - str.Parcului catre sectorul Telecentru.

### 5.6 Alimentarea cu gaze naturale. Reglementări ###
Conform schemei prezentate se prevede inelarea gazoductelor existente presiune medie ∅500 pozat pe str.Petru Rares cu gazoductul existent presiune medie ∅500 pozat pe str.Tudor Vladimirescu:

1. constructia gazoductului presiune medie ∅300 din str.Ismail de la gazoductul existent presiune medie ∅500 din str. Tudor Vladimirescu pina la gazoductul existent p.m. ∅150 din str. Mitropolit Varlaam si gazoduct de presiune medie ∅ 150 din str. Dimitre Cantemir de la gazoductul proiectat presiune medie ∅300 pozat pe str.Ismail pina la gazoductul existent ∅150 din bd.Dimitre Cantemir;

2. constructia gazoductului presiune medie ∅300 din str. Albisoara de la gazoductul existent presiune medie ∅500 din str. Petru Rares pina la gazoductul presiune medie proiectat ∅300 din str.Ismail;

3. constructia gazoductului presiune medie ∅300 pe bd.Dimitre Cantemir in curs de proiectare de la gazoductul existent presiune medie ∅500 din str. Mitropolit Banulescu Bodoni pina la gazoductul presiune medie proiectat ∅300 din str. Ismail;

4. constructia gazoductului presiune medie ∅150 din str.Zaikin de la gazoductul existent presiune medie ∅150 din str. Albisoara pina la gazoductul existent presiune medie ∅ 100 din str.Zaikin;

5. constructia gazoductului presiune medie ∅200 de la gazoductul existent presiune medie ∅500 la intersectia str.Alexandru cel Bun cu str.Serghei Lazo pina la gazoductul existent presiune medie ∅150 pozat catre blocul de locuit din str. Albisoara, 82/8. 

De asemenea, se prevede inelarea gazoductului presiune medie ∅500 din str.Tudor Vladimirescu cu gazoductul existent presiune medie ∅ 500 din str. Bucuresti:

6. constructia gazoductului presiune medie ∅200 din str.Ciuflea de la SRG-9 pina la gazoductul existent presiune medie ∅200 la intersectia str.Ciuflea cu str. 31 August 1989 (gazoductul presiune medie ∅200 este pozat de la gazoductul existent presiune medie ∅ 500 din str. Bucuresti).

In rezultatul celor sus spuse, se asigura alimentarea cu gaze naturale a obiectivelor perspective an. 2010 - 2025:

* Complexul Rezidențial 1 (CR 1) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m. ∅150 din str.Albisoara, ∅100 din str.Zaikin si gazoductele p.m. ce urmeaza a fi proiectate ∅150 din str.Zaikin, ∅150 din punctul de racordare in gazoductul existent ∅500 la intersectia str.Alexandru cel Bun cu str. Serghei Lazo pina la gazoductul existent p.m. ∅150 pozat catre blocul de locuit din str. Albisoara, 82/8;
* Complexul Rezidențial 2 (CR 2) - sursa de alimentare cu gaze naturale sunte gazoductele existente p.m. ∅500 din str.Alexandru cel Bun, ∅150 din str.Albisoara, ∅500 din str.Petru Rares si gazoductele p.m. ce urmeaza a fi proiectate ∅ 150 din str. Zaikin;
* Complexul Rezidențial 3 (CR 3) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m. ∅500 din str.Petru Rares - str.Banulescu Bodoni, ∅200 din str. Ierusalim, ∅100 din str. Puskin incepind cu str.Columna pina la S.A."MOLDOVAGAZ" si gazoductele p.m. ce urmeaza a fi proiectate ∅ 300 din str. Albisoara, ∅ 100 din str. Puskin de la S.A."MOLDOVAGAZ" pina la str.Albisoara;
* Complexul Rezidențial 4 (CR4) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅200 din str.Alexandru Hajdeu - str. Ioan Botezatorul, ∅100 din str. Puskin de la str. Columna pina la S.A."MOLDOVAGAZ" si gazoductele p.m. ce urmeaza a fi proiectate ∅300 din str. Albisoara, ∅300 din bd.Dimitre Cantemir proiectat in perspectiva, ∅100 din str.Puskin de la S.A."MOLDOVAGAZ" pina la str.Albisoara, ∅200 din str.Ioan Botezatorul de la gazoductul existent ∅ 200 la intersectia str.Ioan Botezatorul cu str.Grigore Ureche pina la gazoductul proiectat p.m. ∅300 din str. Albisoara;
* Complexul Rezidențial 5 (CR 5) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅200 din str. A. Hajdeu - str. Cojocarilor - str. Alexandru cel Bun - str. Tighina si gazoductele p.m. ce urmeaza a fi proiectate ∅300 din str.Albisoara, ∅300 din bd.Dimitre Cantemir proiectat in perspectiva, ∅300 din str.Ismail, ∅200 din str.Ioan Botezatorul de la gazoductul p.m. ∅200 la intersectia str. Ioan Botezatorul cu str.Grigore Ureche pina la gazoductul proiectat p.m. ∅ 300 din str.Albisoara;
* Complexul Rezidențial 6 (CR 6) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅500 din str.Mitropolit Dosoftei, ∅400 din str. Mitropolit Petru Movila, ∅125 din str. Toma Ciorba spre SRG-11, ∅150 din bd.Stefan cel Mare si Sfint de la str. Mitropolit Petru Movila pina la blocurile de locuit din bd.Stefan cel Mare si Sfint, 141/2;
* Complexul Rezidențial 7 (CR 7) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅110 - ∅160 din str. Mihai Eminescu, ∅150 din str.Puskin - str.Mitropolit Dosoftei - str.Vlaicu Pircalab spre "Universitatea Libera Internationala a Moldovei" si gazoductele p.m. ce urmeaza a fi proiectate ∅150 din str.Vlaicu Pircalab de la gazoductul existent p.m. ∅110 din str.Veronica Micle pina la gazoductul existent p.m. ∅150 din str.Vlaicu Pircalab;
* Complexul Rezidențial 8 (CR 8) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅150 din str.Tighina - str.Anestiade, ∅200 din str.Lev Tolstoi, ∅100 din str.Bulgara si gazoductele p.m. ce urmeaza a fi proiectate ∅200 din str. Ciuflea, ∅300 din str.Ismail;
Complexul Rezidențial 9 (CR 9) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅ 400 din str. Bucuresti, ∅400 din str.Mitropolit Petru Movila, ∅100 din str.Serghei Lazo;
* Complexul Rezidențial 10 (CR 10) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅400 din str.Bucuresti, ∅325 din str.Mihai Eminescu, ∅150 din str. A. Puskin, ∅100 din str.Bernardazzi;
* Complexul Rezidențial 11 (CR 11) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅500 din str.Bucuresti, ∅400 din str.Pan Halippa, ∅325 din str. Mihai Eminescu, ∅200 din str.Lev Tolstoi, ∅100 din str. Bulgara catre SRG-8;
* Complexul Rezidențial 12 (CR 12) - sursa de alimentare cu gaze naturale sunt gazoductele existente p.m ∅150 din bd.Dimitre Cantemir, ∅150 din str. Anestiade spre SRG-9, ∅100 din str. Albisoara si gazoductele p.m. ce urmeaza a fi proiectate ∅300 din str. Ismail, ∅200 din str. Ciuflea.


### 5.7 Alimentare cu energie electrică ###
#### 5.7.1 Situația existentă ####
Consumatori de energie electrică în zona examinată a or.Chișinău sunt: case de locuit individuale, construcții cu 3-5-10 nivele, școli, grădinițe de copii, instituții de învățămînt, obiective social-culturale și obiective de producere. 

Toți consumatorii se referă la categoriile II și III de fiabilitate a alimentării cu energie electică.

Rețelele de alimentare și distribuție sunt executate în cablu la o tensiune de 6-10kV pe circuite radiale și în buclă. Sursele de energie sunt: ST 110/10-6kV "Sculeni", ST 110/10kV "Malina", ST 110/10kV "Albișoara".

În total sunt 163 de posturi de transformare 6-10/0,4kV cu o capacitate totală a transformatoarelor 147,26 MVA.

#### 5.7.2 Propuneri de proiect ####
Calculul sarcinii electrice a construcției noi de locuințe și obiectivelor social-culturale în zona examinată sunt efectuate în baza:
* instrucțiunilor pentru proiectarea rețelelor electrice urbane (РД34.20.185-94);
* indicătorilor de сost a obiectivelor de alimentare cu energie electrică;
* СНиП 2.07.01-89 "Планировка и застройка городских и сельских поселений";
* altor date inițiale, luate din obiectivele elaborate anterior și documentația normativă.

Sarcină calculată a noilor cartiere locative și a clădirilor social-culturale a fost determinată cu ajutorul sarcinii specifice aduse la barele 0,4kV a posturilor de transformare.

Sarcina calculată totală a energiei electrice a construcțiilor noi în zonele date adusă la barele 10kV a surselor de energie -16,3MVA.

După categoria de fiabilitate a alimentării cu energie electrică consumătorii de energie electrică noi se referă preponderent la categoria a II și parțial la a III.

Sursele de alimentare pentru posturile de transformare 10/0,4kV în zona centrală a orașului in curs de revizuire, sunt: ST 110/10-6kV "Sculeni", ST 110/10kV "Malina", ST 110/10kV "Albișoara" existente.

Pentru acoperirea sarcinilor calculate adăugătoare și a garanta fiabilitatea necesară de alimentare cu energie electrică, este necesar:
* dotarea suplimentară a centrelor de alimentare cu celule de dirivare 10kV;
* сonstruirea liniilor de alimentare în cablu 10kV de la ST 110/10/6kV "Sculeni", ST 110/10 kV "Albișoara" și ST 110/10kV "Malina"; 
* construirea a 38 de posturi de transformare 10/0,4kV;
* construirea rețelelor de distribuție în cablu10kV după schema în buclă (cu rezervarea prin rețele 0,4kV);
* demontarea posturilor de distribuție PD-2 și PD-7 și construirea lor pe terene noi;
* transferarea posturilor de transformare U=6/0,4kV la U=10/0,4kV (111 buc. S inst. tr-lor = 91,96MVA);

### 5.8 Telefonizare și radioficare ###
#### 5.8.1 Situația existentă ####
În zona centrală a orașului (complexele rezidențiale CR1 - CR12) toți abonații sunt asigurați de la STA și substațiile de radiodifuziune existente. Capacitățile existente permit dotarea populației și instituțiilor cu comunicații prin fir și radiodifuziune.

#### 5.8.2 Propuneri de proiect ####
Numărul total de abonați noi a fost calculat în conformitate cu normele în vigoare (a se vedea tab. 5.8.1). Toți abonații noi - 35 mii de numere de telefon se preconizează de a fi conectate la rețeaua de telefonie a orașului prin STA existente, suprafața și sistematizarea cărora permite de a amplasa echipament suplimentar.
Pentru broșarea liniilor magistrale noi de conectare a abonaților la STA este necesar:
* de efectuat pozarea suplimentară a două canale pe ambele părți ale b-dului Ștefan cel Mare - 16 can.km.
* de construit canalizare pentru 6 canale pe ambele părți ale Bulevardului Dimitrie Cantemir - 48 can.km.
* de efectuat pozare suplimentară a două canale de la STA existente până la b-dul Ștefan cel Mare și b-dul D.Cantemir - 16 can.km.

Aceste calcule sunt prezentate în tab. 5.8.1. Partea grafică este prezentată pe plan-schemă de marca RTS.

Calculele date sunt prezentate în tabelele 5.7.1 și 5.7.2, partea grafică este prezentată pe plan-schema de marca REE.

Tabelul 5.7.1 indici tehnico-economici

Tabelul 5.7.2 Calcularea sarcinii

Sarcina totală calculată a obiectivelor proiectate cu evidența coeficientului participării în maximum, alcătuește 14670kW.

#### 5.8.3 Radioficare. Propuneri de proiect ####

Pentru broșarea liniilor magistrale noi de radiodifuziune este necesar:
* de construit două canale pe ambele părți ale Bulevardului Dimitrie Cantemir - 16 can.km.

< Înapoi la [Cuprins](../cuprins.md)