< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)
### 4.5. Dezvoltarea socio-economică ###
#### 4.5.1  Dezvoltarea orașului în calitate de centru administrativ, de afaceri, financiar și de transport ####
Dezvoltarea municipiului Chișinău are ca scop strategic crearea unui centru regional pentru Marea Neagră, cu rol important de relaționare între Eurasia și Sud-Estul Europei, contribuind la integrarea europeană a Republicii Moldova și crearea unui oraș european competitiv. Concomitent cu soluționarea sarcinilor de dezvoltare națională, Chișinăul va juca rolul de capitală, integrată în economia națională și cea europeană, capabilă să asigure condiții favorabile de viață și de dezvoltare a activităților economice. Strategia de dezvoltare teritorială a municipiului Chișinău prevede constituirea unei structuri spațiale uniforme prin dezvoltarea durabilă echilibrată a unităților administrativ-teritoriale din cadrul acesteia. Dezvoltarea pe termen scurt are drept scop eliminarea diferențelor, iar pe termen mediu și lung - de a realiza coeziunea socială și competitivitatea economică, în contextul integrării europene. Previziunile de dezvoltare sunt formulate în conformitate cu factorii avantajoși și competitivi  desemnați:

| factori avantajoși:    | factori competitivi:   |
|------------------------|------------------------|
| * poziția geografică   | * capitalul uman       |
| * resurse naturale     | * potențialul turistic |
| * tradițiile culturale | * domenii specifice    |


Concentrarea și coordonarea patrimoniului economic, social, natural, istoric și cultural face posibil ca mun.Chisinau să devină:
* polul de concentrare economica moderna, care ar fi dominat de activitățile industriale   (zone economice libere, parcuri tehnologice, tehnologii inovatoare) și turism; 
* confortabil pentru living, ceea ce va asigura un mediu curat; 
* direcție atractivă într-o comunitate multiculturală europeană, grație varietății de moduri de expresie: limba, muzica, arhitectura, arheologia, etc.

Principalele obiective strategice de dezvoltare sunt:
* Implementarea infrastructurii de transport în coridoarele europene internaționale;
* Ameliorarea infrastructurii sociale, luând în considerare inter-decontarea serviciului;
* Dezvoltarea sistemului policentric, atractiv pentru investiții și a unui sistem competitiv urban cu dezvoltarea concomitentă a zonelor suburbane;
* Asigurarea condițiilor pentru menținerea, restaurarea și protecția mediului natural;
* Conservarea, protecția și restaurarea patrimoniului istoric și cultural;
* Zonarea funcțională eficientă și bine-echilibrată.



#### 4.5.2 Formarea  atracției arhitecturale a orașului, în concordanță cu peisajul natural ####
Capacitatea continuă de percepție panoramică a construcției sectorului central și a altor zone ale orașului este unul dintre principalele avantaje urbanistice ale Chișinăului, ce îi conferă o personalitate specială și îl diferă de multe alte orașe mari, cu alte condiții de landșaft și de abordare socio-economică și volumetrico-spațială la constituirea mediului urban.

Dezvoltarea teritorială a orașului Chișinău, ca o precondiție pentru dezvoltarea armonioasă și durabilă, prevede următoarele activități:
* punerea în aplicare a structurii policentrice, apropierea serviciilor publice, sociale față de consumatori, în unele cazuri - a locurilor de muncă la formarea unor rețele de centre comerciale, servicii de consum, incubatoare de afaceri, întreprinderi cu tehnologii progresive, care protejează mediul;
* promovarea construcției de locuințe, în special sociale, în noi teritorii pentru a minimiza volumul de investiții publice;
* punerea în aplicare a programelor de reconstrucție în zonele centrale sau în care există clădiri cu un grad avansat de uzură, inclusiv teritorii necesare pentru determinarea zonelor de protecție a obiectivelor de patrimoniu istorice și culturale;
* construcția șoselelor de centură, în special din afară pentru primirea fluxului din al 9-lea Coridor Iinternațional de Transport;
* separarea fluxului trenurilor de călători și de marfă al căilor ferate la construirea  liniilor noi de cale ferată și reorientarea serviciilor respective legate de exploatarea acestora în zona periferică,  eliberând teritoriul nucleului central pentru operarea unor măsuri de protecție a zonei râului Bâc;
* relocarea întreprinderilor industriale din zona centrală și zona rezidențială;
* folosirea teritoriului eliberat  pentru construcția de locuințe sau pentru organizare a zonelor de agrement;
* înverzirea teritoriului din zonele limitrofe ale orașului, în vederea realizării parametrilor europene de asigurare cu zonele verzi, amenajarea modernă a zonelor de agrement existente.




#### 4.5.3 Dezvoltarea economică a capitalei ####

Analiza situației actuale a potențialului industrial al mun.Chisinau a identificat următoarele avantaje și dezavantaje, care au un impact semnificativ asupra dezvoltării integrale socio-economice a capitalei republicii și vor servi drept bază pentru elaborarea direcțiilor prioritare de soluționare complexă a obiectivelor de termen mediu și lung.

TODO - tabel

Dezvoltarea în continuare a capacităților industriale este posibilă prin reconstrucția, modernizarea întreprinderilor în limitele actuale ale formațiunilor industriale, ținând cont de limitările de creștere teritorială a orașului, bazată pe faptul că pe tot perimetrul orașului și municipiului aderă terenuri agricole cu înaltă productivitate. De altfel, se pare, că este cazul organizării pe baza industrială existentă a parcurilor tehnologice, zonelor economice libere, cu un sistem unic de producere și  infrastructură socială.

În vederea ameliorării condițiilor ecologico-sanitare ale mediului înconjurător urban pentru durata planului urbanistic general se prevede eliminarea și retragerea în afara orașului a afacerilor dăunătoare pentru sănătate și igienă, non-core pentru oraș și care nu au spațiu pentru extindere.

Creșterea economică, însoțită de crearea de noi locuri de muncă duce și la o creștere a ocupării forței de muncă. Astfel, nivelul ocupării forței de muncă pentru întregul municipiu va crește de la 47,4% în 2005 la 79% până în 2020. Programul guvernamental prevede crearea în municipiul Chișinău a 40 mii de locuri de muncă noi. În acest caz, principalele domenii de activitate ale economiei municipiului vor atinge  ritmuri diferite de creștere în funcție de tipul activităților și pe perioade. Ponderea principalelor sectoare va rămâne la nivelul anului 2009, și va constitui - 70% servicii, 25% - industrie și 5% - agricultura. Cu creșterea generală a numărului de salariați, respectiv, cel mai mare număr de noi locuri de muncă va apărea în industria de servicii și comerț, cele mai puține - în agricultură. Salariul mediu pe lună pentru fiecare lucrător în domeniul economiei mun.Chișinău va crește de 1,3 ori și va constitui aproximativ 270 USD.

Scenariul optimist de dezvoltare socio-economică se bazează pe următoarele ipoteze: legislație perfectă la nivel macroeconomic și în afaceri, dezvoltarea IMM-urilor, un climat atractiv pentru investiții, reducerea economiei tenebre, investiția în resursele umane, investiția maximă în sfera socială. În conformitate cu dezvoltarea acestei opțiuni  produsul brut regional al mun. Chișinău va crește de 3 ori în comparație cu 2005 și, respectiv de 4 ori, față de anul 2000.

#### 4.5.4 Dezvoltarea infrastructurii sociale ####
Dezvoltarea în continuare a orașului Chișinău, ca centru spiritual al culturii naționale și de știință, de comunicare internațională și stabilitate socio-economică ar crea un nivel sporit de dotare cu toate tipurile de servicii, care ar corespunde standardelor moderne.

Propunerile pentru dezvoltarea infrastructurii sociale cu considerarea noilor condiții socio-economice și urbanistice sunt  bazate pe constituirea în structura orașului a celor două subsisteme de servicii:

* la nivel socio-garantat lângă adăpost, pentru prestatea serviciilor publice și sociale garantate de prima necesitate și intermitente, inclusiv infrastructura socială, axată pe menținerea sănătății umane (fizica, spirituala si intelectuala), pe satisfacerea diverselor sale cerințe și necesități,
* centre la nivel de sistem, pentru furnizarea de servicii urbane cu cerere episodică reprezentate de obiective din sfera de comerț și afaceri, plasate în special în sistemul centrelor urbane, care vizează îmbunătățirea activității de afaceri a populației, dezvoltarea funcțiilor metropolitane, activității comerciale, economiei orașului.

În condițiile actuale se simte necesitatea de introducere a noilor forme și tipuri de obiective de prestare a serviciilor, care ar corespunde cererilor actuale, gradului de mobilitate a tuturor păturilor populației, precum și de diversificare a modului de amplasare a acestora în mediul urban.

O atenție deosebită va fi acordată revitalizării centrului orașului, unde vor avea sprijin prioritar programe de renovare și edificare a construcțiilor noi, acestea fiind catalizator al procesului de dezvoltare, totodată completând caracterul istoric și cultural al fondului existent.

Pentru amplasarea obiectelor mari din sfera de comerț și afaceri de nivel urban și regional, este necesar de a elabora programe sectoriale de dezvoltare a instituțiilor de infrastructură socială în orașul Chișinău. Comerțul cu bunurile cu cerere periodică și episodică, în comun cu întreprinderile de alimentare și serviciile de catering, trebuie să fie prevăzute în cadrul obiectelor mari ale complexului de consum: centre mari de comerț, piețe cu ridicata a produselor alimentare, cumulate cu alte servicii ale infrastructurii sociale. Aceste centre ar trebui să prezinte complexe multifuncționale publice și de afaceri, care formează centre comune orășenești la nivel de sistem, inclusiv centre de plasament a businessului mic.

Bază pentru dezvoltarea culturală în continuare în țară, este crearea unor centre complexe distractive contemporane cu elemente de sport și recreere, atât în zonele urbane, cât și în cele rurale. Amplasarea obiectelor de cultură în cadrul centrelor publice ale localităților vizează deservirea populației din zona respectivă de influență, în funcție de destinația obiectului - la nivel de microraion, oraș, raion, regional, național. În acest caz, se va lua în considerare multifuncționalitatea clădirilor, ținându-se cont de activitățile desfășurate și crearea unui centru cultural în fiecare localitate.

Punerea în aplicare completă a acțiunilor enumerate va crea o nouă imagine a orașului european modern, comod pentru rezidenții Chișinăului și atractiv pentru vizitatorii capitalei.

#### 4.5.5 Dezvoltarea infrastructurii turismului ####
Mun.Chișinău are o istorie bogată și cultura etnică multiseculară, care definește identitatea și unicitatea sa. Aceasta se bazează pe un peisaj unic și condiții naturale favorabile, respectul pentru aceasta a multor generații, care au creat o anumită anvergură de dezvoltare, astfel, lăsând posibilitatea de dezvoltare durabilă în continuare în conformitate cu cerințele timpului. Capacitatea continuă de percepție panoramică a construcției sectorului central al orașului este unul dintre principalele avantaje urbanistice ale Chișinăului, ce îi conferă o personalitate specială deosebită de multe alte orașe mari, cu alte condiții de landșaft. Aceste tendințe și tradiții ar trebui să fie respectate și dezvoltate cu scopul de a transforma orașul într-un centru turistic de nivel european și de a îmbunătăți confortul de trai al populației.

Activitatea economică în domeniul turismului implică o utilizare eficientă a patrimoniului turistic, creșterea veniturilor valutare în bugetul de stat, cooperarea fondurilor financiare a agenților economici din industria turismului, în vederea dezvoltării sale și crearea de locuri de muncă. Unele tipuri de turism - de afaceri cognitive, (acestea constituie o proporție semnificativă din numărul total de turiști), contribuie la dezvoltarea industriei serviciilor și a unor sectoare de producere a accesoriilor legate de turism.

La ora actuală de-a lungul drumurilor magistrale de importanță națională și regională, în zone atractive pentru turism sunt construite hoteluri mici și chempinguri cu includerea restaurantelor, parcărilor auto, locurilor pentru recreere și distracții, creând condiții mai confortabile pentru călătorie.

Republica Moldova tot mai ferm dovedește dreptul său de prezență la drumul european al vinului. Rutele vinului, pornind de la capitală, acoperă aproape toată țara. Majoritatea agențiilor de turism, numărul cărora ajunge peste 250, au inclus tururi de vin în planurile sale. Agroturismul este un augur special al timpului. Totul cu ce trăiesc astăzi oamenii de la sat, cu îndeletnicirile lor, cu modul de a trata și a distra - vor fi apreciate cu siguranță de oaspeții Moldovei. Drumul Vinului în Republică este un drum larg spre prosperitate și popularitate în lume a tuturor punctelor forte, resurselor și bogățiilor naționale, cu care se mîndrește țara noastră.

Tradițiile populare sunt o parte integrantă a patrimoniului cultural, iar diversitatea etnică nu numai că nu a distrus modul de trai tradițional, dar i-a dat un aspect nou, creând un conglomerat original de obiceiuri, rituri și ritualuri, păstrate până în ziua de azi.

Formarea rutelor, excursiilor, programelor de vizitare a obiectivelor turistice, furnizarea serviciilor de bază și auxiliare constituie tehnologia serviciilor de turism, și anume acestea țin de formarea unui produs turistic specific pentru a satisface cererea de servicii turistice.

Instruirea lucrătorilor din industria turismului se realizează de instituțiile de stat și private de învățămînt secundar profesional, instituțiile de învățământ secundar special și superior, precum și de instituțiile specializate de educație profesională continuă, acreditate în conformitate cu legislația.

Principalele măsuri ale politicii de stat în domeniul turismului sunt:
* o dezvoltare integrată, echilibrată și durabilă a turismului intern și internațional;
* promovarea Republicii Moldova pe arena internațională ca destinație turistică a țării;
* integrarea politicii de dezvoltare a turismului în structura economiei țării;
* includerea activelor turismului național în sistemul internațional de turism;
* crearea și punerea în aplicare a reglementării de stat a activităților turistice;
* crearea unui climat investițional favorabil pentru atragerea investițiilor locale și străine în domeniul turismului;
* asigurarea accesibilității la informația în domeniul turismului, accesul larg la statisticile de turism, utilizarea fondurilor de turism;
* estimarea optimă a potențialului turistic;
* introducerea diversității calitative și cantitative a traseelor turistice și ofertei de servicii, cu condiția creării unei infrastructuri adecvate, inclusiv de transport (drumuri, hoteluri, campinguri, etc);
* protecția mediului înconjurător și conservarea obiectivelor turistice.

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)