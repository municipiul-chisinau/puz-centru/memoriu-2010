< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)
### 4.3. Populația, situația demografică ###
În conformitate cu construcția locativă nouă propusă de prezentul document și caracterul de populare a fondului locativ, numărul populației zonei Centru către anul 2025 va atinge 76,5 mii oameni, iar densitatea - 108 om/ha. 

Tabelul 4.3.1

Numărul prognozat al populației în zonă poate servi drept ofertă pentru propunerea de a separa teritoriul examinat într-o unitate administrativ-teritorială de sine stătătoare. 

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)