< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)
### 4.1. Reglementări urbanistice ###
#### 4.1.1   Structura administrativ-teritorială ####

Teritoriul or.Chișinău este divizat în 5 sectoare administrative, nucleul istoric aparține la 3 din ele (Rîșcani, Buiucani și Centru). Prevederile PUZ propun modificarea structurii administrativ-teritoriale a orașului împărțind tot teritoriul în 6 sectoare: Botanica, Ciocana, Râșcani, Buiucani, Telecentru și Centru (a se vedea planșa 4.1.4 "Modificarea structurii administrativ-teritoriale", sc 1:2000). 

 Sectorul Centru propus va include nucleul istoric al orașului și teritoriile aderente la nord, nord-vest, sud-vest și sud fiind limitat de: str.Pan Halipa, str.Hîncești, de-a lungul str.A.Mateevici până la hotarul Parcului Valea Morilor, teritoriul Parcului Dendrariu (str.E.Coca), str.I.Creangă, str.Columna, str.M.Viteazul, str.Albișoara, str.Ismail, str.Calea Basarabiei, Gara Feroviară, hotarul Parcului Valea Trandafirilor, str.Melestiu, str.Pan Halipa.

Se propune ca sectorul Telecentru să fie format din teritoriul sectorului Centru actual (cu excluderea teritoriului care se va include parțial în sectorul Centru propus și parțial în sectorul Botanica) și teritoriul or.Codru. Din teritoriile sectoarelor Rîșcani și Buiucani se vor exclude teritoriile care se vor include în sectorul Centru propus. Sectorul Botanica se va extinde din contul teritoriului sectorului Centru actual. Sectorul Ciocana rămâne neafectat.

La elaborarea prezentului document au fost puse la bază 2 principii care oferă oportunități la atingerea scopurilor propuse:
* zona centrului istoric a orașului a fost examinată utilizând noțiunea de Complexe Rezidențiale (CR) ca unitate convențională de divizare a teritoriului, renunțându-se la noțiunea de microraioane. Așadar, pentru o concepere mai clară, teritoriul de sistematizare a fost divizat în 12 Complexe Rezidențiale, care la rândul său au fost împărțite în cartiere limitate de străzi pe perimetru. Deoarece un șir de clădiri fiind monumente se află în afara centrului istoric, se propune de a extinde limita acestuia, introducând următoarele obiective: biserica Sf. Împărați Constantin și Elena cu monumente funerare (3,7 ha), biserica Sf.Tiron din str.Ciuflea cu monumente funerare (2,8 ha) și Academia de Științe a RM, Complexul "Eternitate" (19,2 ha) - (Cimitirul Armean și Memorialul gloriei militare), teritoriul cuprins între str.A.Mateevici și Parcul Valea Morilor (24,3 ha) care conține multe obiective de patrimoniu și complexul Institutului de medicină (2,8 ha). Teritoriul cu construcția contemporană cu multe nivele de-a lungul str.Albișoara, precum și a complexului locativ din bd.Ștefan cel Mare,3 se va exclude din zona nucleului istoric. Astfel teritoriul centrului istoric va atinge 573,3 ha (a se vedea planșele  4.1.4 "Modificarea structurii administrativ-teritoriale" și 4.1.2 "Schema patrimonială", sc 1:2000) și limitele acestuia vor fi determinate de străzile: Hîncești, sectorul de-a lungul str.A.Mateevici până la hotarul Parcului Valea Morilor, I.Stere, A.Șciusev, teritoriul complexului Institutului de medicină, M. Viteazul, Columna, Sf.Andrei, Sfatul Țării, Romană, A.Pușkin, Albișoara, Mazarache, Romană, V.Alecsandri, Gr.Ureche, Ismail, Ștefan cel Mare și Sfânt, I.Vasilenco, L.Tolstoi, Pan Halipa. A mai fost inclus pentru examinare teritoriul a 2 complexe rezidențiale, ce nu sunt parte a nucleului istoric: la nord CR1(mărginit de str.Albișoara și M.Viteazul) și sud-est CR12 (mărginit de str.Albișoara, bd.Iu.Gagarin, bd. C.Negruzzi) și fîșia cuprinsă între str.Albișoara și zona căii ferate cu includerea acesteia, mărginite de bd.Iu.Gagarin, bd.C.Negruzzi și str.M.Viteazul, astfel teritoriul examinat atingând suprafața de 833,6 ha.

* deoarece liniile roșii ale PUG-ului actual afectează grav fațadele construcțiilor existente de-a lungul străzilor din zona centrală și a monumentelor de patrimoniu, se propune ca acestea să corespundă cu liniile de aliniament ale construcțiilor capitale existente, ceea ce va lăsa posibilități de manevrare la efectuarea intervențiilor urbanistice și de construire în zona solicitată, cu excepția liniilor roșii ale bdului D.Cantemir și a străzilor Ismail, Sfatul Țării și V.Alecsandri (ultimele două pe segmentul între str.Albișoara și bd. D.Cantemir).

#### 4.1.2 Zonificarea funcțională ####
Valoarea istorico-artistică a centrului istoric și funcția capitalei republicii influențează considerabil concepția arhitectural-spațială a teritoriului său și tipul restructurării sale.

Dezvoltarea centrului orașului  se reduce la două direcții:
* perfecționarea centrului istoric format, adaptarea structurii și construcției sale la cerințele contemporane în permanentă actualizare;
* sistematizarea zonificală  a teritoriului centrului istoric, aprecierea posibilității privind încorporarea noilor complexe publice și locuibile în mediul format.

În orașul format este importantă preluarea funcțiior care, de regulă, prezintă o stabilitate înaltă, luarea în considerare a legăturilor existente între zone separate și magistrale de transport.

Funcțiile principale ale centrului orașului sunt administrativ-politice, de afaceri, cultural-educative, comercial-menajere etc.

Funcțiile de producere, precum și cele locative, se referă la funcții auxiliare, deși construcția locativă se referă la element necesar al structurii centrului.

Se propune următoarea zonare a teritoriului (a se vedea planșa 4.1.1 "Schema funcțională", sc 1:2000):

_Zona dotărilor_ care va include un spectru larg de funcții - administrativ-politice, de afaceri și comerț, cultural-educative, prestări servicii se va concentra, în majoritate, de-a lungul axelor compozițional-spațiale principale ale centrului - bdelor Ștefan cel Mare și Sfânt și Gr.Vieru, precum și în fâșia între străzile Ismail, Albișoara și T.Vladimirescu. Legăturile acestei zone cu alte zone ale centrului vor fi facilitate prin introducerea acceselor pietonal-cicliste (pe străzile N.Iorga, V.Pârcălab și Mitropolit Dosoftei) și pietonale (pe străzile V.Micle și A.Corobceanu), ceea ce va avea impact pozitiv asupra mediului și va favoriza gradul de confort al mediului ambiant.

_Zonele rezidențiale_, care ocupă cea mai mare suprafață din Centru istoric vor avea următoarea structură:

* zona rezidențială cu nivelare limitată, care va coincide cu sectorul ce concentrează cea mai mare parte a monumentelor de patrimoniu; regimul de înălțime - 1-3 nivele; construcția nouă în această zonă se reglează de regulamentul aferent PUZ-ului, sunt permise activități de protecție și restaurare a monumentelor de patrimoniu, reabilitare, reparare capitală și modernizare a fondului construit și infrastructurii tehnico-edilitare;
* în zona rezidențială cu nivelare variabilă este permisă amplasarea solitară a obiectivelor noi cu înălțimea nu mai mare de 4-7 nivele și aspect exterior, care să armonizeze cu mediul în care se înscrie;
* zona rezidențială cu obiective de comerț va include blocuri locative cu spații comerciale încorporate care deja și-au ocupat spațiul istoric de-a lungul bdului Ștefan cel Mare și str.Ismail;
* zona rezidențială multietajată este zona construcției contemporane cu regimul de înălțime 7-9 nivele;
* zona accentelor arhitectonice - obiective izolate sau complexe arhitecturale cu rol de accent urbanistic cu înălțimea de 9 nivele și mai mult.

_Zonele sau formațiunile industriale_, în majoritate își vor păstra destinația funcțională actuală, fiind consolidate, modernizate, retehnologizate referitor la activitățile de bază, echiparea cu transport, infrastructură edilitară și comunicații. Întreprinderile industriale din zona riverană Bâcului vor fi reamplasate pe etape, iar teritoriul eliberat va fi ocupat de zona dotărilor. 

Tabelul 4.1.1 - Bilanțul funciar al zonelor funcționale



#### 4.1.3  Zonificare  patrimonială ####

Sunt propuse următoarele zone patrimoniale (a se vedea planșa 4.1.2 "Schema patrimonială" sc 1:2000):
* zona de protecție arheologică -  biserica Nașterea Maicii Domnului cu teritoriu adiacent din str.Măzărache, în care se interzice orice intervenție afară de cercetări arheologice;
* zona de rezervație patrimonială va cuprinde teritoriul cu cea mai mare concentrație a patrimoniului nucleului istoric în perimetrul străzilor V.Alecsandri, București, Sfatul Țării, A.Mateevici și Complexul Cimitirului Central Armean- zona cu intervenții strict reglamentate pentru fiecare cartier aparte în confomitate cu regulamentul local și studiile arhitectural-urbanistice speciale privind restructurarea mediului urban format;
* zona de protecție istorică - 2 sectoare în partea de jos a regiunii istorice, ce include cea mai veche parte a orașului, unde s-au păstrat fragmente de planificare istorică neregulată veche și un șir de monumente arhitecturale și istorice: primul sector se află în zona colinei Pușkin, în limitele ei stabilite, unde sunt concentrate majoritatea monumentelor istorice și arhitecturale legate de aflarea poetului în Chișinău.  Al doilea sector - zona amplasată între str.Pușkin și str.Ismail. Imagine caracteristică o posedă partea centrului între aceste 2 sectoare, formată conform proiectului lui A.Șciusev și construită în anii 50-80 ai secolului trecut. Intervențiile în zonă se vor reglementa prin Planuri urbanistice de detaliu, elaborate în conformitate cu regulamentul local aprobat și cu considerarea construcției istorice existente.  
* zona de protecție arhitectural-istorică va cuprinde fâșia centrală mărginită de străzile Columna, M.Viteazul, A.Mateevici cu includerea sectorului amplasat mai sus (până la parcul Valea Morilor), Ismail și teritoriul bisericii Sf. Împărați Constantin și Elena cu monumente funerare. 
Intervențiile în această zonă vor respecta următoarele condiții:
  * atenție deosebită trebuie acordată executării arhitecturale a clădirilor ce formează perimetrul cartierelor, păstrării caracteristicilor de bază a imaginii specifice sectorului.
  * înălțimea clădirilor aderente la monumente arhitecturale nu trebuie să depășească înalțimea acestora, iar clădirile ce formează construcția de-a lungul străzilor să nu depășească înălțimea la cea mai înaltă construcție existentă din cartier. În interiorul cartierelor, executarea arhitecturală a clădirilor este mai liberă și ele pot avea înălțimi mai mari.
* zona de renovare cu protecția elementelor de patrimoniu se va situa de-a lungul bdului Cantemir propus și se va extinde în partea "orașului de jos" în vecinătate nemijlocită cu zona de protecție istorică. Intervențiile în teritoriu se vor executa avînd în vedere includerea în structura cartierelor și complexelor rezidențiale a monumentelor de arhitectură amplasate pe terenuri aparte. Pentru păstrarea dimensiunilor construcției străzilor istorice, înalțimea clădirilor nu trebuie să depășească 2-3 etaje - 7-10 m pe perimetrul străzilor. În interiorul cartierelor și magistralelor noi pot sa fie ridicate construcțiile multenivelate.

#### 4.1.4  Caracteristica fondului imobiliar ####
Construcția zonei Centru este formată din clădiri și edificii cu funcții de locuit și nelocuibile amplasate în teritoriul dat. Cca 2 mln. m. p. de suprafață utilă totală revine obiectivelor cu funcții nelocuibile și 1674 mii m.p. - de suprafață locativă totală, ce constituie cca 10 % din fondul locativ total al orașului.  

Clădiri publice de unicat sunt amplasate de-a lungul bulevardelor și străzilor centrale (clădiri ale instituțiilor de stat și publice, obiective de cultură și artă, bănci, centre distractiv-comerciale, instituții de învățământ superior și alte instituții de nivel republican, municipal, raional și internațional).

Procentul de ocupare a teritoriului (POT) în zona Centru constituie 26%. În contextul Complexelor Rezidențiale acest indice oscilează în limitele: de la 15% în CR1 până la 35% în CR11.

Coeficientul de utilizare a teritoriului (CUT) variază nesemnificativ în limitele Complexelor Rezidențiale, atingând valoarea maximă de 0,94 în CR12.

În afara funcțiilor susnumite zona Centru este locul de reședință pentru 64 mii cetățeni urbani. Fondul construit, istoric constituit al Complexelor 
Rezidențiale poartă caracter neomogen cu nivelare variată. Această diversitate a construcției din teritoriu este reflectată pe Plan-schema din planșa 4.1.3  "Caracteristica fondului imobiliar"sc 1:2000.
Fondul locuibil al zonei Centru poate fi reprezentat astfel:
* 27% - case cu 1-2 nivele (minietajate)
* 22% - blocuri cu 3-6 nivele (midietajate)
* 31% - blocuri cu 7-9 nivele (multietajate)
* 20% - blocuri cu 10 nivele și mai mult (înalte).

Clădirile înalte - cu 10 nivele și mai mult se amplasează în CR 1-7 în care ponderea acestora constituie de la 22% până la 30% din fondul locativ, iar în CR 1 - 50%.

Ponderea construcției locative minietajate nu depășește 1/4 din fondul locativ total în CR 2,3,4,6,7,8.

CR 9,10,11 în "orașul de sus" se caracterizează prin construcție mini- și midietajată, unde ponderea fondului locativ cu 1-2 nivele atinge 75%.

O astfel de amplasare a fondului locativ are impact asupra densității acestuia în teritoriul zonei:
* cea mai înaltă densitate > 7000 m.p./ha - în CR12
* cea mai joasă           < 1000 m.p./ha - în CR7, unde sunt amplasate majoritatea clădirilor guvernamentale, instituțiilor publice și distractive.

Densitatea medie în "orașul de sus" constituie de la 1600 până la 2000 m.p./ha, în "orașul de jos" - de la 2000 până la 3000 m.p./ha.

Fondul nelocuibil al zonei Centru, reprezentat prin clădirile instituțiilor administrației publice, de comerț, de știință și învățământ, oficiilor, complexelor distractive sunt concentrate de-a lungul bdului Ștefan cel Mare.

Circa o jumătate din suprafața utilă nelocuibilă revine CR 6,7 și 8. Majorarea fondului nelocuibil se realizează atât din contul construcției noi, cât și schimbând destinația spațiilor locative de la parterul blocurilor locative în încâperi cu funcții nelocuibile cu organizarea ulterioară a întreprinderilor de comerț, prestări servicii etc, ceea ce este preconizat în propunerile privind construcția nouă de perspectivă.

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)