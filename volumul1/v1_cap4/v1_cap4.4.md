< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)
### 4.4. Spațiul locativ ###
Reeșind din potențialul teritorial al zonei Centru oferta de construire a fondului locativ cu  nivelare variabilă pentru perioada anilor 2010-2025 poate arăta în felul următor:  

Tabelul 4.4.1

În tab 4.4.1 este subînțeles că fondul șubred existent în complexele  rezidentiale va fi reconstruit sau înlocuit, în dependență de construcția deja constituită și doleanțele proprietarului. 

În conformitate cu oferta de construire propusă a fondului locativ nou, construcția locativă către a.2025 va arăta astfel:

Tabelul 4.4.2

În proiect se prevede construirea clădirilor nelocative cu diverse destinații amplasate izolat sau încorporate în blocuri locative. Dinamica clădirilor și încăperilor nelocative este urmărită în tab 4.4.3:

Ca rezultat al acțiunilor propuse în proiect, construcția zonei Centru către a.2025 se va caracteriza prin următorii indici:

Tabelul 4.4.4

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)