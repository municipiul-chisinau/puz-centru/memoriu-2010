< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)
### 4.2. Structura drumurilor și de transport ###
#### 4.2.1 Rețeaua de drumuri și străzi ####
Chișinăul  este pînă în prezent singurul oraș din Republica Moldova, care a păstrat fragmente din vechea tramă stradală medievală. Acest fapt se datorează structurii urbane  medievale și abundenței monumentelor de arhitectură în Centrul Istoric.

Rețeaua de drumuri și străși, precum și edificiile ridicate în zona centrală a municipiului, reprezintă o moștenire valoroasă a culturii secolelor trecute.

Chișinăul reprezintă localitatea cu cel mai mare număr de edificii istorice care s-au păstrat, și care totodată reflectă practic toate problemele existente în Republica Moldova  în domeniul protecției patrimoniului cultural construit.

Schema propusă  a rețelei de drumuri și străzi din zona Centru prevede o structură de clasificare clară, simplă și compactă a străzilor magistrale și locale (a se vedea planșa 4.2.1/1,2 "Rețeaua de drumuri și străzi" sc 1:2000).

Concomitent se propune de a interzice categoric staționările provizorii pe străzile cu rol de magistrală de interes urban sau raional, de a prevedea "buzunare" pentru oprirea transportului public, de a asigura marcarea rațională a părții carposabile, de a aplica metode moderne de reglare a circulației de tipul "Unda verde".

Rețeaua de străzi locale aderă la cele magistrale, trebuie eliberată de fluxurile directe și utilizată pentru deservirea construcțiilor amplasate în spațiul inter-magistral, pentru parcaje organizate de scurtă durată. Ieșirile de pe aceste străzi tebuie să fie preferențial cu cotitură dreapta. Acest principiu de comasare a cvartalelor "rutiere" va permite de a spori capacitățile de trafic a schemei stradale și a diminua deficitul parcărilor organizate.

Soluționarea problemelor existente de trafic în zona centrală prevede, de asemenea, dezvoltarea infrastructurii fluxurilor adiacente, menite să asigure conenexiunea directă a raioanelor principale (str.Gh.Asachi - Pan Halipa, bd.Gagarin - Cantemir, str.Vadul lui Vodă - Ismail, str. Calea Ieșilor -bd.Ștefan cel Mare și Sfînt). Sub acest aspect este nevoie de a soluționa problema nodului rutier str. Calea Moșilor - str.Ismail, ce va asigura redislocarea fluxurilor. 

Pentru aceasta este necesar:
* Sporirea capacității de trafic a rețelei stradale;
* Diminuarea influenței negative asupra structurii magistralelor din zona centrală a fluxurilor rutiere nespecifice;
* Sporirea securității circulației rutiere și a pietonilor;  
* Reconstrucția srăzilor exzistente în scopul sporirii capacităților de trafic a acestora din contul extinderii părții carosabile și aducerii altor parametri tehnici pînă la valoarea maxim posibilă;
* Micșorarea considerabilă a impactului negativ a fluxurilor de transport asupra teritoriilor adiacente. 

Pentru perspectivă sunt propuse magistrale de interes urban: str. C.Stere, str. A.Mateevici, str. Mitropolit G.Bănulescu-Bodoni, str. A.Pușkin, str. V.Alexandri, str.Ismail, bd. C.Negruzzi, bd. D.Cantemir, str. Albișoara, bd. G.Vieru, bd. Renașterii Naționale, Calea Moșilor, str. Mihai Viteazul.

Magistralele de interes raional sunt propuse: str.Columna, str.S.Lazo, str. Sfatul Țării, str.31August 1989, str.București, str. A.Șciusev, str.M.Kogălniceanu, str.Armenească, str.Ciuflea.

Bd.Ștefan cel Mare și Sfînt este stradă principală urbană.

Realizarea legăturii directe între centrul orașului și raioanele de Nord pe str. Ismail rămîne foarte actuală. Magistralele existente (bd.Renașterii și str.A.Russo) sunt suprasolicitate și nu corespund exigențelor ecologice moderne.

Pe porțiunea între str. Stere și V. Alecsandri magistrala prevede mișcarea în ambele sensuri și lățimea părții carosabile pînă la 11,5m.

Finalizarea bd. D.Cantemir de la str. Ismail pînă la str. M.Viteazul are o importanță primordială ca magistrală de interes urban cu circulație rutieră. Ea va prelua o sarcină rutieră semnificativă, eliberînd parțial bd. Ștefan cel Mare și Sfînt, str. Albișoara, str. Columna, va soluționa multiplele probleme legate de deservirea cu transport a părții inferioare a zonei centrale, va ameliora starea ecologică în coridorul de trasare a străzii din contul deschiderii magistralei, diferențierii fluxurilor după tipul mijloacelor de transport, sporirii  capacităților de trafic. Strada va deveni o magistrală nouă modernă cu parametri opționali, amenajări contemporane. 

Referitor la nodurile de transport putem spune următorul lucru - se propune pentru perspectivă nod de transport în 2 nivele la intersecția străzilor bd.Dimitrie Cantemir și str.Mihai Viteazul.

Al 2-lea nod denivelat se propune cu unele modificări la intersecția str.M.Viteazul și str.Albișoara.

Pentru a descărca str.Albișoara se propune de a construi un drum  paralel acestei străzi de la str.V.Alexandri pînă la str.M.Viteazul. De asemenea pe acest segment este propus un nod în două nivele puțin modificat la intersecția cu bd Grigore Vieru-Renașterii Naționale și treceri peste r.Bâc pentru a asigura manevre la stânga la intersecțiile străzilor Buna Vestire, Petru Rareș cu str. Albișoara.

Rămîne intact nodul de la intersecția str. Albișoara, bd.D.Cantemir și str.Negruzzi. Rămîne neschimbat și nodul de transport cu sens giratoriu de la str. Pan Halipa

#### 4.2.2 Linii roșii ale străzilor ####
Pentru perspectivă sunt calculați parametrii principali a magistralelor. Luînd în considerare experiența mondială de proiectare a părților istorice a orașelor europene și a liniilor roșii, ultimele au fost trasate pe aliniatul a construcțiilor existente cu excepția str. Ismail de la str.Albișoara - str.Bernardazzi și bd. D.Cantemir de la str. Ismail- str. Mihai Viteazul.

Planul liniilor roșii este prezentat pe planșa 4.2.2/1,2 "Linii roșii a străzilor" sc 1:2000.

#### 4.2.3 Organizarea circulației rutiere ####
Străzile A.Șciusev și Stere pe porțiunea bd. Ștefan cel Mare și Sfînt și str. A.Mateevici sunt magistrale de interes urban cu circulație rutieră reglată.

Strada Columna este magistrală de interes raional cu circulație unisens.

Strada București este magistrală de interes raional cu circulație rutieră unisens și funcționează în paralel cu str. M. Kogîlniceanu. 

Strada 31 August 1989 este magistrală de interes raional cu circulație rutieră unisens, posedă pe unele sectoare parametri suficienți și poate fi extinsă pînă la 2-3 benzi pe diverse porțiuni. Strada M. Kogîlniceanu este magistrală de interes raional cu circulație rutieră unisens. Se propune de a o extinde în lățime la 3 benzi.

Străzile S.Lazo și Sfatul Țării sunt magistrale de interes raional cu circulație rutieră unisens,funcționează în direcții opuse și asigură legătura dintre raioanele de Sud cu cele de Nord. 

Strada V.Alecsandri, pe porțiunea între str. A.Mateevici și Pan Halipa, funcționează unisens din oraș, ceea ce sporește capacitatea de trafic.
Ulterior, există posibilitatea de a se mai anexa o bandă pe acest segment de magistrală, deoarece aici nivelul de solicitare a depășit deja posibilitățile sale.

Bd. D.Cantemir se propune de a fi o magistrală de interes urban cu circulația transportului în dublu sens cu amplasarea semafoarelor la interesecții cu străzile: Tighina, Armenească, V. Alexandri, A.Pușkin, Mitropolit G.Bănulescu-Bodoni, Sfatul Țării, S. Lazo (a se vedea planșa 4.2.3/1,2 "Organizația circulației rutiere" sc 1:2000).



#### 4.2.4 Accese pietonale și de biciclete ####
Pînă în prezent circulația pietonală la nivelul orașului se desfășoară pe trotuare și în zone pietonale delimitate dar amplasate izolat și fără legături între ele. Zonele pietonale delimitate ca atare sunt reprezentate de: Piața Marii Adunări Naționale, Scuarul Catedralei Înălțării Măicii Domnului, Parcul Ștefan cel Mare și Sfînt, precum și Aleea de pe bd. Grigore Vieru etc. Toate acestea sunt situate în zona istorică. În prezent lipsesc facilități pentru circulația bicicliștilor. Avînd în vedere gradul înalt de motorizare a municipiului, în deosebi a zonei centrale, strategia administrației locale privind dezvoltarea mijloacelor alternative de transport trebuie să cuprindă un concept coerent pentru încurajarea circulației bicicletelor. Acest concept trebuie să cuprindă: crearea rețelei de piste de biciclete care să cuprindă principalele trasee precum și prezentarea profilelor pistelor, corelate cu cele pentru pietoni, în așa fel încît să nu se ocupe integral trotuarele cu noile piste.

În locurile de acumulare masivă a pietonilor și traversare a străzilor cu circulație rutieră intensă, se propune edificarea pasajelor pietonale: bd.Ștefan cel Mare și Sfînt, str. Columna, str. Mitropolit G. Bănulescu-Bodoni, str.Vlaicu Pîrcălab, str.Ismail, str.Albișoara, bd.Cantemir, str.Ciuflea. Se propune străzile V. Micle și A.Corobceanu să fie accesibile numai pentru pietoni (a se vedea planșa 4.2.5/1,2 "Accese pietonale și de biciclete" sc 1:2000).

Deoarece centrul orașului este foarte aglomerat, se propune amenajarea pistelor pentru biciclete pe următoarele străzi: Vlaicu Pîrcălab - de la str.Mitropolit Varlaam pînă la str.Bernardazzi, Iorga - de la str. Mitropolit Dosoftei  pînă la str. A.Mateevici, Mitropolit Dosoftei - de la str.Mihai Viteazul pînă la str.Mitropolit G.Bănulescu-Bodoni. 

Piste pentru biciclete se propun și pe str.Mitropolit Varlaam - de la str.Ismail pînă la str. A.Pușkin, precum și pe bd. Gr. Vieru și în Scuarul Catedralei.

#### 4.2.5 Obiecte de deservire a transportului #### 
Odată cu majorarea numărului de transport auto mai sunt necesare parcaje muleetajate și subterane, garaje (a se vedea planșa 4.2.6/1,2 "Obiecte de deservire a transportului" sc 1:2000).

Parcajele multeetajate propuse  sunt prezentate în tab 4.2.1:

Tabelul 4.2.2



#### 4.2.6 Tansportul public urban ####
Transportul public urban al mun.Chișinău actualmente este reprezentat prin 4 tipuri - troleibuz, autobuz, maxi-taxi și taxi. Activitatea microbuzelor a fost orientată, în general‚ spre dublarea rutelor de troleibuz și autobuz. 

La o etapă anumită aceasta a influențat pozitiv asupra satisfacerii necesităților în traficul de pasageri. Însă, schema de circulație a unora pe rutele de troleibuz și autobuz, existența unui număr mare de unități, care simultan se deplaseză pe linie (peste 1500), principiul de funcționare "oprire la cerere", au generat sarcini suplimentare peste schema stradală a zonei centrale. Din această cauză pentru maxi-taxi a fost propusă amenajarea stațiilor fixate de așteptare.

Troleibuzele transportă anual peste 184 mln. pasageri. Troleibuzul este un transport ecologic și corespunde normelor de deservire a pasagerilor. Se bazează pe o rețea suficient de ramificată a liniilor de contact, lungimea totală a cărora constituie 523,6 km sau 17,2 % de la lungimea tramei stradale .

Actualmente, în oraș funcționează 23 rute de troleibuz, din care 22 traversează zona centrală. Trei parcuri de troleibuze conțin 240 unități.

Garnitura rulantă are un grad înalt de uzură fizică și morală, necesită  renovare.

Rețeaua de transport electric asigură legătura între zonele urbei prin centru și se trasează pe străzile: M.Viteazul, Mitropolit G.Bănulescu-Bodoni, A. Pușkin, Ismail, Ciuflea, București, M.Cogîlniceanu, A. Mateevici, V. Alecsandri și bulevardele  Ștefan cel Mare și Sfînt și Negruzzi. Concomitent este necesar de a focaliza atenția pe complicațiile care apar  în circulația și manevrarea mijloacelor de transport cu gabarite mari, la care, în special, se referă troleibuzul, pe străzi înguste, la intersecții dreptunghiulare cu raze mici de cotituri caracteristice zonei centrale. Aceste considerente generează necesitatea optimizării schemei de circulație în conformitate cu capacitatea străzilor, cu minimizarea numărului de manevre, soluționîdnd astfel cele mai stringente probleme de organizare a mișcării troleibuzelor.

În municipiu funcționează 30 rute  de autobus din care 27 circulă și în zona centrală. 

Autogara, amplasată pe str.Mitropolit Varlaam se referă la categoria de obiectiv al transportului extern deservind traficul internațional, republican și suburban. Odată cu construcția autogării de Nord, amplasate pe str.Calea Moșilor, a fost preluată o parte din sarcina autogării Centru. Reorientarea completă spre Gara de Nord permite de a degaja considerabil trama stradală în partea Sud-Est a zonei centrale (străzile Ismail, Tighina, Columna, Alexandru cel Bun) și se reflectă pozitiv asupra situației traficului de pasageri cu transportul urban public.

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul IV. Reglementări](../v1_cap4.md)