< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)

### 3.6. Obiective social-culturale, administrative, ştiinţifice, de educaţie, învăţământ, sportive şi de producere ###

Instituţiile administrativ-publice şi de afaceri sunt concentrate preponderent de-a lungul axelor de planificare principale - bd. Ştefan cel Mare şi bd. Renaşterii, precum şi pieţele aderente.

Instituţii culturale şi artistice constituie şi ele baza nucleului centrului şi sunt plasate pe bd. Ştefan cel Mare şi str. Mitropolit Dosoftei.

Instituţiile medicale sunt distribuite pe tot teritoriul centrului şi constau din 6 spitale municipale, spitalul de urgenţă, spitalul de boli infecţioase, cîteva policlinici raionale şi specializate.

Instituţiile de învăţămînt şi ştiinţifice, precum şi instituţii de afaceri şi publice, sunt concentrate în general de-a lungul axelor de planificare principale - bd. Ştefan cel Mare şi bd. Renaşterii, str. Banulescu-Bodoni, str. Puşkin. Instituţiile sunt amplasate în cladiri istorice precum şi în blocuri noi.

Întreprinderile comerciale şi de alimentare publică definesc tipul instituţiilor ce se includ în complex locativ (magazine, cafenele, cantine incluse) de-a lungul magistralelor principale şi a zonelor de pietoni. Unele instituţii comerciale şi de alimentaţie publică sunt amplasate în clădiri aparte şi în încăperi încorporate şi anexate. Piaţa comercială contemporană s-a format pe o piaţă mare în centrul oraşului, pe care anterior era amplasată hala.

Obiectivele sportive sunt concentrate în lunca r.Bîc, la joncţiune cu sectorul Rîşcani. Aici s-a amplasat manejul de atletică uşoară, Universitatea de educaţie fizică şi sport. În nucleul central al oraşului este încadrat stadionul "Dinamo". 

Obiective de producere În partea centrală a oraşului sunt amplasate o serie de întreprinderi industriale: Fabrica de Cofetarie "Bucuria", "AROMA" S.A., Combinatul Poligrafic "Chişinău" S.A., Fabrica de mobilă "Viitorul", Fabrica de Confectii "Steaua" S.A., Fabrica de confectii "Ionel" S.A-, Fabrica de mobilă "ICAM" S.A. "Ponti" S.R.L., "Steaua-Reds" S.A., "Cristina-Mold-Rom-Simpex" S.R.L.

Lista şi parametrii clădirilor în care sunt amplasate instituţiile publice şi ale autorităţilor administraţiei  de nivel republican şi municipal, precum şi obiectivele de comerţ şi prestări servicii sunt arătaţi în Anexa nr.2 şi planşa 2.2.1 "Analiza după destinaţia clădirilor" sc 1:2000.

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)