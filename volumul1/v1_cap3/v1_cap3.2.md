< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)

### 3.2. Evaluarea arhitectural-peisajistică ###
Chişinăul a apărut lîngă r.Bîc în nodul natural al landşaftului ambiant. Aici valea este cea mai îngustă, amplasată între două coline mari, iar pantele colinelor înalte din jurul văii reprezintă locurile cheie în structura sa spaţială. Erau valorificate încă din antichitate. Astăzi, în locurile acestea cele mai deschise chiar la marginea colinelor abrupte se află biserici construite încă în sec. XVIII: Mazarachi pe malul drept şi sf. Constantin şi Elena pe malul stîng.

Pînă la începutul sec XIX oraşul se dezvolta lent pe malul drept, valorificînd terenuri tot mai depărtate de rîu şi mai ridicate. Un salt considerabil s-a observat în următorul secol.  Către începutul sec.XX a fost valorificată întreaga colină domoală la poalele căreia s-a format nucleul istoric al oraşului. 

Pe panta lină a ei s-a plasat partea principală a sectorului istoric al oraşului. Astăzi pe acest teritoriu se află centrul oraşului Chişinău. Colina se află în centrul oraşului şi joacă un rol deosebit de important în structura şi imaginea sa. 

Sectorul istoric şi centrul sunt strîns legate în plan fucnţional, vizual, compoziţional cu teritoriile adiacente. Acestea sunt valea r.Bîc şi colinele de pe malul său stîng, precum şi  văile  aderente la colina centrală din vest şi sud. Spre deosebire de teritoriul colinei construit dens, teritoriile adiacente pantelor abrupte ce îl limitează formează un semi-inel verde. În el se includ: parcul-dendrariu, parcul "Valea Morilor", cimitirul "Armenesc" şi masivul plantaţiilor verzi la sud de centru. 

Multe spaţii verzi conţine valea r.Bîc, ce cuprinde centrul din partea opusă. Ea este strîns legată cu plantaţiile verzi pe pantele colinei Rîşcani şi a altor coline şi în văile între ele.

Astfel, relieful, plantaţiile verzi, suprafeţele de apă joacă un rol important în organizarea spaţială şi funcţională a părţii centrale a oraşului şi a teritoriilor adiacente.

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)