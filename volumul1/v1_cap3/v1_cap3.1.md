< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)

### 3.1. Condiţiile naturale ###
#### 3.1.1   Condiţiile geomorfologice ####
Teritoriul centrului oraşului Chişinău este situat pe partea malului drept al rîului Bîc, limitat la nord-vest şi la sud-est de văile pîrîului Durleşti şi a pîrîului Malina Mica. El se împarte în următoarele elemente geomorfologice: luncile inundabile ale rîului Bîc şi ale afluenţilor săi, panta terasei de şes a rîului Bîc, fâşia cumpenilor afluenţilor şi o parte din pantele văilor afluenţilor menţionaţi mai sus la periferia sectorului.

Expoziţia priotritară a teritoriilor pantelor este orientată înspre cursurile de apă: a) a teritoriului principal - nord-estică (înspre Bîc), b) a marginii de vest - vestică (înspre pîrîul Durleşti), c) a marginii de sud-vest - sud-vestică (înspre pîrîul Malina Mică). Teritoriul principal "a", conform condiţiilor de relief (panta pîna la 10%),  e favorabil pentru construcţii, cu excepţia unor sectoare tranzactorii între lunca inundabila şi terasa de şes (10-20%). Teritoriile pantelor văilor "b" şi "c" (10-20% şi mai mult) se referă la teritorii nefavorabile şi foarte nefavorabile, iar sectorul "b" prezintă şi pericol de alunecări de teren. Scurgerea apelor de suprafaţă este îngreunată, deoarece cartirele ansamblului de construcţii vechi împiedică direcţia acestei scurgeri.

#### 3.1.2   Condiţiile geologico-inginereşti ####
În sectorul descris, roci de bază sunt calcare treatice, argilele şi nisipurile. Răspîndire mai mare o au argilele glomerulare de alevrit, argilele nisipoase şi cu intercalaţii subţiri de nisip. În plus, paralel cu panta, se întind fîşii cu lăţimea de 50-400 m şi lungime de la 500 m  de nisip fin granulat cuarţos-micaceu cu densitate medie sau de argile dense stratificate. Cele din urmă se gasesc, de asemenea, în luncile inundabile.

Rocile de bază sunt acoperite de un strat de sedimente cuaternare, reprezentate de argile nisipoase, argile, soluri nisipo-argiloase şi nisipuri. Grosimea sedimentelor de suprafaţă  variază de la 5-10 m în sectoarele estice şi nord-estice ale teritoriului pînă la 10-15 m şi mai mult în celelalte sectoare.

Partea estică a teritoriului e acoperită de nisipuri aluviale granulate, pulverulente cu intercalaţii subţiri de argile nisipoase. În partea vestică, precum şi în fîşia la sud de str. Cahul,  cu lăţimea de cca. 300 m, sunt răspîndite sedimente aluvionale de terasă: argilele nisipoase, solurile nisipo-argiloase, nisipurile, argilele, mai rar pietriş ce se interstratifică. Pe rocile de bază a părţii de sud a pantei şi a fîşiilor de cumpănă, se găsesc sedimente nisipos-lutoase de terasă, iar pe ele - argile nisipoase şi soluri nisipos-lutoase eolo-diluviale loessoide. În zonele alunecărilor de teren, rocile de bază sunt acoperite de roci nisipo-lutoase morfolite de acumulări din alunecări de teren, iar în zonele de luncă inundabilă - de roci aluviale contemporane: mîluri şi sedimente nisipos-lutoase. Circa jumătate din teritoriul de bază este acoperit de soluri rambleate. La aşa terenuri se referă, de exemplu, sectorul între str.Columna, lunca Bâcului, str.Petru Rareş, str.Vlaicu Pârcălab, grosimea maximă a rambleului fiind 2,4 m. Grosimea rambleului ajunge pînă la 7-9 m - în sectorul final al str. Ştefan cel Mare şi Sfînt, lîngă panta spre valea pîrîului Durleşti.

Sarcinile de calcul asupra rocilor la adîncimea 1,5-2 m la construcţia pe terenuri indundabile pot fi adoptate în limitele 10-12 N/m2 , iar la construcţie deasupra terenurilor inundabile - 15-25 N/m2 . Seismicitatea de calcul pe teritoriu este de 7 şi 8 grade.

#### 3.1.3 Condiţiile hidrogeologice ####
Pe teritoriul centrului a fost stabilită o ridicare considerabilă a nivelului apelor freatice în regiunile cu o adîncime anterioară mai mare a aşezării lor. A diminuat suprafaţa terenurilor, unde această adîncime pe harta schematică depăşea 4 m. De exemplu, izobata de 4 metri ce se afla în regiunea bd. Ştefan cel Mare, se află la moment mai sus pe pantă cu cca 100-650 m.

La teritorii cu adîncimea mai mică a nivelului stabilit al apelor freatice, sub 2 m, se referă: 
* 1) lunca inundabilă între str. Mihai Viteazu şi str. Zaikin; 
* 2) cartierul pe Bulevardul Dimitrie Cantemir - Albişoara; 
* 3) regiunea bd. Ştefan cel Mare de la cladirea AŞRM până la şi str. S Lazo (fîşii cu lăţmea pînă la 400 m la sud-vest de bulevard şi pînă la 450 m la nord-est). În luncă inundabilă se observă înmlăştiniri.

#### 3.1.4 Condiţiile ecologice ####
Din cauza reliefului neomogen se creează zone microclimaterice specifice, în care condiţiile meteorologice determină gradul diferit de poluare a aerului în cartierele oraşului.

Clima este continental-moderată cu iarnă scurtă şi vară lungă călduroasă. Particularităţile climaterice ale teritoriului sunt favorabile.
Indicele poluării aerului în Chişinău este ridicat, principalii poluanţi fiind dioxidul de azot (1.106 din concentraţia maximă admisibilă), praful (0,048 CMA), oxidul de carbon (0,56 CMA), dioxidul de sulf (0,046 CMA), fenol (0,2 CMA), formaldehida (0,39 CMA).

Sursele principale de poluare a aerului în Chişinău sunt: transportul auto, obiectele termoenergetice, în măsură mai redusă - întreprinderile industriale (Uzina de tractoare, Combinatul de prelucrare a pielii, Uzina de piele, Combinatul de carne Chişinău, CET-1 şi CET-2) care, deşi au programe şi volume reduse de producere, sunt situate în sectoarele centrale ale oraşului şi nu dispun de zone sanitare de protecţie. Transportul intensiv reprezintă sursa principală a poluării fonice.

Râul Bâc face parte din categoria obiectelor acvatice cu destinaţie piscicolă, în care calitatea apei în amonte de oraş este satisfăcătoare, iar în aval gradul de poluare este sporit.

Conţinutul de produse petroliere, compuşi de cupru, azot amoniacal, nitraţi, nitriţi şi detergenţi depăşeşte de câteva ori indicii CMA pentru apele cu destinaţie piscicolă. În lunca râului pe teritoriul oraşului sunt două zone mari industriale depăşite de timp (Sculeni şi Uzinelor) amplasate de-a lungul căii ferate.

Acestea poluează zona centrală a oraşului, aflată în calea rozei vânturilor prioritate, precum şi apele râului. Cca 40 întreprinderi industriale şi depozite sunt amplasate în zona de protecţie a Bâcului, în care se scurg toate apele reziduale comunale sau rezultate în urma proceselor productive şi de la precipitaţii.

Scurgerile de apă din reţelele de distribuţie şi canalizare au condus la fenomenul de subinundaţie manifestat prin creşterea considerabilă a nivelului apelor freatice în multe zone din oraş. Pe teritoriul mun. Chişinău sunt 49 de obiecte industriale periculoase din punct de vedere chimic, radioactiv şi explozibil.

Inundaţiile afectează periodic 4 sectoare cu o suprafaţă totală de 22,9 km2. Acestea sunt produse de ploile torenţiale şi topirea zăpezilor, care duce la creşterea nivelului apelor şi revărsarea acestora.

Cutremurele de pământ cu intensitatea de 7-8 grade au în or. Chişinău o periodicitate de 30-40 de ani.

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)