< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)

### 3.5. Zonificare funcțională ###
Zona examinată este amplasată în limitele teritoriului administrativ al sectoarelor Buiucani, Centru, Râșcani și include centrul - nucleul istoric al mun.Chișinău, cu statut de zonă protejată de importanță națională, mărginită de străzile A.Mateevici, Albișoara, Ismail, Ștefan cel Mare și Sfânt, Ciuflea, L.Tolstoi, Pan Halipa și constituie o suprafață de 619 ha. Limitele zonei examinate au fost extinse, adăugându-se teritorii aderente la nord, sud-est și sud-vest și biserica Sf. Împărați Constantin și Elena cu monumente funerare, atingând suprafața de 833,6 ha. 

În proiectul dat a fost utilizată  noțiunea de Complexe Rezidențiale (CR) ca unitate convențională de divizare a teritoriului, renunțându-se la noțiunea de microraioane. Zona examinată a fost divizată în 12 Complexe Rezidențiale, care la rândul său au fost împărțite în cartiere limitate de străzi pe perimetru. Fiecare cartier a fost analizat aparte pe suport topografic la sc 1:500 pe următoarele compartimente: 
* destinația clădirilor; 
* formele de proprietate;
* regimul de înălțimi;
* gradul de uzură;
* monumentele de patrimoniu cu zonele protejate.

Aceleași compartimente au fost reflectate pe planșele respective la sc 1:2000.  

Zonificarea funcțională în centru este organizată în așa fel, încît sa fie creată complexitatea și diversitatea mediului. Aceste calități sunt atinse cu ajutorul principiilor de zonificare multifuncțională mixtă. Renunțarea la metodele zonificării "curate" nu exclude posibilitatea nominalizării teritoriilor cu funcții preponderente - 
* locuire; 
* comerț; 
* de producere; 
* infrastructura socială; 
* teritorii destinate transportului; 
* zonele de odihnă și de agrement. 

(A se vedea planșa 2.2.1/1 "Analiza după destinația clădirilor" sc 1:2000).

Teritorii pentru locuire se caracterizează prin consolidarea funcțiilor principale ale habitației. Actuale pentru orașul Chișinău, cât și pentru zona centru sunt reconstruirea fondului locativ existent, reabilitarea tehnică, termică și mansardarea caselor de locuit existente; asigurarea cu elemente ale infrastructurii edilitare, de transport și comunicații, sporirea nivelului de amenajare a cartierelor de locuințe. Totodată, în zonă sunt precăutate și rezerve de teritoriu pentru construcția fondului locativ nou.

Funcția industrial-productivă. Complexul industrial al municipiului este stabilit de 19 formațiuni industriale, cu o suprafață de cca. 2800 ha care au inregistrat mai mult de 1000 de întreprinderi din diferite domenii de activitate economică (sfera industrială, transport, energetică și comunală). Aceste formațiuni sunt amplasate în partea de nord-est a orașului (11 formațiuni). Unele ocupă teritorii importante în zona de protecție a râului Bâc pe segmentul ce traversează partea centrală a orașului, poluând zona centrală a orașului, aflată în calea rozei vânturilor prioritare.

Infrastructura socială, prevăzută în PUG-ul precedent nu a fost realizată în măsura planificată, evidențiindu-se depășirea ritmurilor construcției de locuințe față de edificarea sistemului de utilități și servicii publice. Astfel, în prezent se evidențiază îndepărtarea serviciilor sociale de locul de trai al populației, supraîncărcarea zonei centrale cu obiective de menire publică, dotarea neuniformă a sectoarelor cu obiecte cu destinație educațională, medicală, protecție socială, centre sportive, muzeistice, culturale, etc. 

Infrastructura de transport - în structura urbanistică a orașului reprezintă un sistem în care se evidențiază patru zone planimetrice principale: 
* sud-vest (Botanica - Telecentru), 
* nord-vest (Buiucani - Sculeni), 
* nord-est (Râșcani, Budești, Poșta Veche), 
* centru (zona industrială "Sculeni", Valea Trandafirilor, Valea r.Bâc). 

Factorii de bază ce influențează menținerea acestei structuri sunt landșaftul, elementele infrastructurii tehnice, sistemul existent de străzi magistrale, calea ferată, obiectele importante ale complexului energetico-industrial. Acestea complică organizarea legăturilor rutiere durabile între formațiunile structurale ale mediului urban. Legăturile zonei centrale cu alte elemente structurale se asigură pe puținele străzi radiale, cum ar fi M. Viteazul, bd. Renașterii, bd. Gagarin, bd. Dacia, str. V.Alecsandri, Sfatul Țării, Calea Ieșilor, I.Creangă, fapt care a dus la consolidarea diametrelor, intersectate de fluxuri mari de transport intraurban și la suprasolicitarea rețelei de drumuri și străzi din centrul capitalei.

Spații verzi - Pe teritoriul capitalei sunt determinate următoarele tipuri de spații înverzite: 

* de folosință comună; 
* cu acces limitat; 
* cu profil specializat; 
* cu funcții utilitare. 

Spațiile verzi de folosință comună din zona examinată constituie circa 17 ha. La acestea se referă parcurile:
* Grădina publică Ștefan cel Mare, 
* Parcul catedralei,

precum și câteva scuaruri.

Zona cu profil specializat este constituită de grădina Muzeului Național de Etnografie și Istorie a Naturii - una din cele 4 arii naturale protejate de stat amplasate în capitală, și cimitirul central, care de asemenea se află sub protecția statului.  

Zone cu acces limitat sunt reprezentate de terenuri sportive, stadioane, terenuri pentru copii etc. Pentru a întregi carcasa verde a capitalei este necesar de a extinde spațiile verzi, în special în zona riverană râului Bâc și crearea unei zone de agrement cu toată infrastructura necesară. Această activitate trebuie realizată concomitent cu inventarierea fondului zonelor verzi, parcurilor, scuarurilor, reabilitarea și modernizarea acestora.

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)