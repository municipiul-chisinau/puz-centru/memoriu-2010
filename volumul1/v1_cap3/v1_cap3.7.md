< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)

### 3.7. Situaţia socio-economică ###
Ţinând cont de concentraţia semnificativă a instituţiilor, potenţialul ştiinţific şi de producere, instituţiile administrative şi de credit din or.Chişinău, dezvoltarea economiei republicii, în general, depinde direct de capitală. Partea principală a BIP (60%), potenţialul industrial (50,4%), investiţii străine (69,2%), impozitele percepute revin mun.Chişinău. Datele comparative ale indicilor economici principali ai mun.Chişinău pentru perioada anilor 2005-2009 sunt reflectaţi în  fig.1. 

Domeniul afacerilor municipiului Chişinău reprezintă la nivel de indicatori generali cca. 27,7 mii de întreprinderi, 129 mlrd. lei vânzări nete şi 11,8 mlrd. lei profit brut anual şi un personal mediu scriptic de 322,5 mii de oameni.(tab.3.7.5). 

În a. 2005, din 22 mii de întreprinderi înregistrate ca raportori financiari în mun. Chişinău, 96% sunt întreprinderi ale micului business. Acestea din urmă asigură un sfert din vânzările nete, 38% din personalul total angajat şi 14% din profitul brut. În perioada anilor 2005-2009 a sporit numărul întreprinderilor cu 6 mii, cu 59,4 mlrd. lei vânzări nete sau cu 85%, cu 8,5 mlrd. lei profit brut anual sau de 2,5 ori, personalul mediu scriptic anual cu 22,6 mii de oameni sau cu 7,5%. Numărul întreprinderilor business-ului mic a sporit cu 6,2 mii şi a constituit 97,6% din numărul total al întreprinderilor. 

În totalul vânzărilor şi profiturilor un lider incontestabil este comerţul (respectiv 46,1% şi 21% în 2005, respectiv 45,9% şi 31,6% în 2009), urmat de sectorul transport-comunicaţii (13,2% şi 41,4% în 2005 respectiv 12,1% şi 17,5% în 2009), ultimul fiind şi cel mai profitabil sector (a se vedea fig.2). Industria prelucrătoare are şi ea un aport important în formarea bazei economice a oraşului (ponderi de 16,2% şi, respectiv, 17,8% în 2005 şi respectiv 12,4% şi 9,6% în 2009 în vânzări şi profit). Dominarea sectorului terţiar în economie este un fenomen obişnuit pentru localităţile urbane moderne. 

Teritoriul or.Chişinău în hotarele existente constituie 12301 ha. Centru este elementul de unificare administrativă, economică, socio-culturală şi a infrastructurii edilitare, iar perfecţionarea lui arhitectural-urbanistică influenţează asupra altor sectoare ale oraşului. 

Profilul teritorial-economic al oraşului este marcat de dominarea sectoarelor Centru cu o pondere totală de 27,6% în cifra de afaceri (vânzări nete) şi Râşcani cu 27,4%. Urmează Buiucani cu16,7% şi Botanica cu16,1%. Sectorul Ciocana ocupă ultima poziţie cu 12,3%. La capitolul vânzări raportate la populaţie, clasamentul arată astfel: oraşul - 100%: Centru - 180%, Râşcani - 122 %, Buiucani - 91%, Ciocana - 71%, Botanica - 61% . Sectorul Centru este lider la comerţ, al doilea la servicii şi pe ultima poziţie la industrie. Sectorul Râşcani - primul la servicii şi construcţii şi penultimul la industrie. Sectorul Buiucani este primul la industrie, al treilea la comerţ şi penultimul la servicii. Botanica este printre lideri la industrie (poziţia II) şi construcţii (III) şi printre codaşi la comerţ şi servicii. Ciocana este a treia la industrie şi pe ultimul loc la celelalte poziţii. 

Unul din factorii de bază care influenţează dezvoltarea socio-economică a oraşului este industria. 

Volumul producţiei industriale pentru ianuarie-decembrie 2009 a însumat 10098,6 mil. lei in preţuri curente, numărul mediu al angajaţilor din industrie constituie 70,9 mii oam., inclusiv 31,9 mii oam. în businessul mic. 

Întreprinderile industriale cuprinse în cercetare statistică lunară au marcat o diminuare de 19,1% a volumului producţiei industriale. Activitatea de producere a întreprinderilor cuprinse în cercetare statistică lunară, evaluată în preţurile medii ale anului precedent, a fost determinată preponderent de activitatea întreprinderilor din industria prelucrătoare, cărora le-au revenit 74,7% din volumul total de producţie.

Industria municipiului este reprezentată preponderent de cea prelucrătoare, ponderea căreia în totalul producţiei industriale a înregistrat în ultimii 4 ani o scădere de 4,4 puncte procentuale şi a atins 80,4% în 2009. Ramura energetică a sporit cu 4 puncte procentuale ca urmare a stabilizării consumului de energie electrică şi apă de către întreprinderile industriale. Ponderea industriei extractive s-a dublat, deşi ca mărime absolută ea alcătuieşte mai puţin de 1,2% din total. Aceasta a avut loc preponderent ca urmare a creşterii importante în sfera construcţiilor (respectiv cererea de piatră, nisip, ciment etc. a sporit considerabil).

În industria prelucrătoare un lider incontestabil îl reprezintă ramura produselor alimentare şi a băuturilor (pondere de 28,1% din totalul producţiei industriale în 2009). Ponderi relativ importante (6,4%) a sporit în comparaţie cu anul 2005 cu 2,3 puncte stabile înregistrează aşa domenii ca fabricarea hârtiei şi a cartonului, poligrafia şi reproducerea materialelor informative, articolele din cauciuc şi materiale plastice. Succesele ramurilor menţionate se datorează dinamicii industriei alimentare şi sferei construcţiilor. Industria constructoare de maşini şi aparate este în continuă restrângere (ponderea de 4,6%) şi aceasta în condiţiile când delocalizări importante în alte zone ale ţării nu se înregistrează (cum se întâmplă, spre exemplu, în cazul ramurii confecţiilor şi textilelor). În ultimii 5 ani volumul producţiei industriale a sporit aproximativ cu 65%. Indicii principali a întreprinderilor cu activitatea principală industrială pentru perioada anilor 2004-2008 sunt reflectaţi în tab.3.7.6.

La momentul actual în oraş există 19 formaţiuni industriale cu o suprafaţă de cca. 2800 ha, în care sunt amplasate cca. 1,0 mii de întreprinderi din diferite domenii de activitate economică (sfera de producere, transport, energetică şi comunală). Amplasarea acestor formaţiuni este concentrată în zona nord-est a oraşului (11 formaţiuni). O parte din ele ocupă teritorii importante în zona de protecţie a r.Bâc în zona centrală a oraşului. Respectiv, apare problema delocalizării lor într-o perspectivă previzibilă. 

O mare parte din întreprinderile industriale au trecut prin proceduri de restructurare, divizare şi reorganizare, totuşi o soluţie integră de revigorare a zonelor industriale ale oraşului aşa şi nu a fost găsită. O anumită perioadă a fost vehiculată ideea creării unor zone economice libere. Astfel, Legea cu privire la zonele antreprenoriatului liber a fost adoptată încă în anul 1993, în baza căreia a fost aprobată Hotărârea Guvernului nr.13 din 10 ianuarie 1998, în care se indica oportunitatea înfiinţării şi funcţionării zonelor antreprenoriatului liber - a parcurilor industriale, ştiinţifice, tehnologice, inovatoare. Au fost enumerate şi întreprinderile industriale din municipiu pe baza cărora se planificată înfiinţarea de parcuri industriale şi tehnologice-ştiinţifice, printre care: "Sigma" S.A.; "Micron" S.A.; "Semnal" S.A.; "Tracom" S.A.; "Alfa" S.A.; "Topaz" S.A.; "Tebas" S.A., precum şi centrul "Lises" al Academiei de Ştiinţe şi Universităţii Tehnice. Implementarea în practică a planurilor respective are loc însă destul de anevoios. Principala cauză - lipsa unui mecanism juridico-economic coerent care ar permite potenţialilor investori să demareze rapid activităţile de organizare şi lansare a afacerilor. 

În partea centrală a oraşului sunt amplasate un şir de întreprinderi industriale şi de producere mari: Fabrica de cofetărie "Bucuria"; "AROMA" S.A.; Combinatul poligrafic "Chişinău" S.A.;  Fabrica de mobilă "Viitorul"; Fabrica de confecţii "Steaua" S.A.; Fabrica de confecţii "Ionel" S.A.; Fabrica de mobilă "ICAM" S.A.; "Ponti" S.R.L.; "Steaua-Reds" S.A.; "Cristina-Mold-Rom-Simpex" S.R.L.; "Hidropompa" S.A.; "Vibropribor" Î.S.

Teritoriile industriale deja formare, în general, corespund normativelor în vigoare, concomitent, însă, apar un şir de probleme ecologice, ce ţin de asigurarea zonelor de protecţie sanitară, minimizarea evacuărilor nocive, sau stabilirea nivelului impactului tehnogen. 

Vânzările cu amănuntul. Sectorul comercial, ca parte a sistemului dezvoltat a deservirii socio-comunale, constituie un factor principal în organizarea abitaţiei populaţiei şi într-o oarecare măsură contribuie asupra condiţiilor de abitaţie, muncă şi agrement. 

În mun.Chişinău sunt concentrate un număr semnificativ de obiective comerciale, care îndeplinesc atât funcţii de capitală cât şi funcţii de urbă. Acestea sunt centrele comerciale mari cu întreprinderi de alimentare publică, cultură şi agrement, complexele de piaţă, saloane prestări servicii, ateliere cu diverse tipuri de servicii, precum şi un şir de întreprinderi mici de nivel de microraion şi raion a business-ului mic şi mediu. Capacitatea totală a obiectivelor comerciale, conform situaţiei de la 01.01.2009 constituie 259,7 mii m2 suprafaţă comercială, ceea ce constituie în mediu 331m2 la 1000 locuitori. În sectorul Centru sunt amplasate 77 mii m2 suprafaţă comercială, sau 27% din toată capacitatea municipiului. Concomitent, este necesar de accentuat faptul că, peste 50% din numărul total al obiectivelor comerciale sunt amplasate în construcţii provizorii (gherete, chioşcuri), ceea ce se reflectă negativ atât asupra calităţii deservirii, cât şi nivelului de amenajare. Amplasarea obiectivelor comerciale, deseori, se realizează fără a se respecta normativele urbanistice, ceea ce contribuie la pierderi neîntemeiate ale teritoriului şi utilizarea neraţională a acestuia. 

In ianuarie-decembrie 2009 prin unităţile comerciale au fost vândute bunuri de consum în valoare de 11121,2 mil. lei, cu 5,3 la sută mai puţin faţă de ianuarie-decembrie 2008, ce constituie 55,8% din totalul pe ţară.

Vânzările cu amănuntul au înregistrat în ultimii 4 ani creşteri de 35,3%, 34,2%, 31,1%, 31,1% şi diminuând cu 5,3 % în 2009. Au fost vândute produse alimentare în sumă de 3841,5 mil. lei, înregistrând o creştere de 7,0% faţă de perioada similară din anul precedent. Mărfuri nealimentare 
s-au vândut în valoare de 7279,7 mil. lei, cu 11,5 la sută mai puţin faţă de ianuarie-decembrie 2008.  Ponderea volumului vânzărilor de mărfuri nealimentare din totalul de vânzări a constituit în unităţile comerciale 65,5%.

Volumul de vânzări cu amănuntul, realizat de unităţile comerciale în luna decembrie 2009 a însumat 1168,0 mil. lei, diminuând cu 7,3 la sută comparativ cu luna decembrie 2008.

Tabelul 3.7.1 - Indicii volumului vânzărilor cu amănuntul

| Grupul de produse | 2005 | 2006 | 2007 | 2008 | 2009 |
|-------------------|------|------|------|------|------|
|                   |      |      |      |      |      |
|                   |      |      |      |      |      |
|                   |      |      |      |      |      |

Explicaţia acestui fenomen rezidă în creşterea ponderii clasei medii (din contul reprezentanţilor business-ului mic, profesioniştilor liberi şi gasterbaiterilor) şi apariţia motivaţiei serioase de a amenaja locuinţa şi a "ieşi în lume" după ani buni de sărăcie. Este evident că, valul vânzărilor a crescut rapid dar nu va mai continua în acelaşi ritm (cel puţin, în următorii 3-5 ani), ca urmare a faptului că multe din produsele menţionate sunt de uz îndelungat (respectiv, cumpărăturile repetate vor mai aştepta).

Indicii principali ai circulaţiei mărfurilor vânzărilor cu amănuntul în sectoarele municipiului sunt reflectaţi în tab.3.7.2. 

Tabelul 3.7.2 - Vînzări de mărfuri cu amănuntul, pe forme de proprietate, pe sectoarele municipiului în 2008

Circulaţia mărfurilor din vânzările cu amănuntul în sectorul Centru a constituit în 2008 - 3339,1 mln. lei sau aproximativ 28% din circulaţia vânzărilor cu amănuntul pe oraş. 

Servicii. Cinci tipuri de servicii (transportul de pasageri, poşta-telecomunicaţii, servicii comunale, învăţământ, alimentaţia publică) au deţinut în comun o pondere de 82,5% în totalul încasărilor în 2008. Acest nivel a fost menţinut de către serviciile susnumite pe parcursul întregii perioade 2005-2009 ca urmare a 2 factori de bază: rolul de servicii de primă necesitate a celor 5 tipuri de servicii, dezvoltarea slabă a altor servicii pentru populaţie. Un argument în plus în susţinerea opiniei de insuficientă dezvoltare a sectorului terţiar (cererea depăşind oferta) îl reprezintă preţurile galopante de pe piaţă. Acest fapt este confirmat de discrepanţă vizibilă între indicii în preţuri curente faţă de cei în preţuri ale anului precedent (a se vedea fig.3).

Stocurile de mărfuri existente in unităţile comerciale la 1 ianuarie 2010 s-au cifrat la 3224,5 mil. lei sau cu 7,3% mai mult faţă de 1 ianuarie 2009. 

Volumul serviciilor cu plată prestate populaţiei de către unităţile oficial înregistrate în ianuarie -decembrie 2009 s-a cifrat la 5829,2 mil. lei, micşorindu-se cu 14,2 la sută faţă de perioada similară din anul precedent.

Investiţii capitale. În ianuarie-decembrie 2009 pentru desfăşurarea activităţii investiţionale a economiei municipiului, din contul tuturor surselor de finanţare, au fost utilizate 4629,0 mil. lei (in preţuri curente), diminuând cu 52,4% faţă de realizările perioadei similare ale anului precedent.

Volumul investiţiilor capitale a crescut în perioada 2005-2008 de 2,6 ori. Principalul "furnizor" de investiţii în 2009 a fost sectorul întreprinderilor (cca. 79,2% inclusiv 25,5 % investorii străini), bugetele de stat şi locale asigurând în comun cca. 6%, iar restul - populaţia şi creditele bancare.

Evoluţia structurii investiţiilor pe sfere de utilizare arată o clară creştere a ponderii sferei nonproductive (de la 20,2% în 2000 - la aproape 19,1% în 2008). Principalele direcţii de investiţie în acest sens au fost unităţile de comerţ şi alimentaţie publică şi reţelele de infrastructură, preponderent de gaz, apă-canal, termice, drumuri. Este vorba mai degrabă de o reabilitare a reţelelor degradate pe parcursul anilor de tranziţie decât de proiecte serioase noi (a se vedea fig.4).

În ce priveşte structura ramurală a investiţiilor capitale, majoritatea acestora (cca. 80% în 2008) au fost concentrate în patru ramuri: tranzacţii imobiliare (32,5%), transporturi şi comunicaţii (15,3%), industria prelucrătoare (12,5%) şi comerţ (19,1%). 

Tabelul 3.7.3 - Structura ramurală a investiţiilor capitale

Pentru construcţia locuinţelor şi altor obiecte de menire social-culturală în ianuarie-decembrie 2009 au fost însuşite 2332,2 mil. lei mijloace investiţionale ( 50,4% din volumul total al mijloacelor însuşite).

În construcţia de locuinţe cu ritmuri mai sporite s-a realizat construcţia locuinţelor individuale. Din 320,7 mii m2 de suprafaţă totală a locuinţelor date în folosinţă în ianuarie- decembrie 2009 - 45,0% a revenit beneficiarilor individuali. Darea în folosinţă a locuinţelor pe forme de proprietate în ianuarie-decembrie 2009 se caracterizează astfel:

Tabelul 3.7.4

Din suprafaţa totală a locuinţelor date în folosinţă în ţară, 71,8% au fost construite în municipiul Chişinău.

Pentru efectuarea lucrărilor de construcţii-montaj au fost valorificate investiţii în sumă de 2521,3 mil. lei (54,5% din total) şi cu 53,9% mai puţin decât în perioada similară din anul precedent. Pentru procurarea maşinilor, utilajului, au fost valorificate investiţii în mărime de 1705,1 mil. lei, pentru procurarea mijloacelor de transport - 229,1 mil. lei.

Au fost puse în funcţiune mijloace fixe în valoare de 4525,3 mil. lei, din care de către agenţii economici cu proprietate privată - 2001,5 mil. lei, străină şi a întreprinderilor mixte - 1627,9 mil. lei, publică - 728,5 mil. lei, mixtă (publică şi privată), fără participare străină - 167,4 mil. lei.

Dezvoltarea infrastructurii municipale în perioada de raport a fost remediată printr-un şir de obiecte sociale date în folosinţă: magazine cu suprafaţa comercială de 9616,7 m2; drumuri cu lungimea de 1,4 km; întreprinderi de alimentare publică - 80 locuri; linii pentru circularea troleibuzelor - 15,9 km; hoteluri - 40 locuri.

TODO 

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)