< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)

### 3.3. Situaţia urbanistică şi spaţial-volumetrică
Mediul urban al sectorului istoric al Chişinăului este foarte neomogen. Diversitatea formată în unele din regiunile sale este determinată de specificul dezvoltării istorice a oraşului, condiţiile naturale ale sectorului şi specificul funcţionării sale în calitate de partea centrală a capitalei. În limita sectorului central se pot deosebi 8 regiuni de bază.

Cea mai mare şi semnificativă în plan arhitectural este regiunea ce cuprinde nucleul centrului. El s-a format în partea de sus a colinei centrale, pe teritoriu cu planificare regulată şi concentrează cele mai multe monumente arhitecturale si istorice, clădiri semnificative şi caracteristice imaginii oraşului sec.XIX - înc.sec.XX. În limitele sale mediul are caracter întegral, iar unele regiuni şi grupuri de cartiere se deosebesc prin ansamblu definit, posedă calităţi artistice înalte. Tot aici sunt amplasate cele mai multe clădiri semnificative contemporane. Ele nu au distrus reţeaua istorică a cartierelor.

În partea de jos a regiunii istorice, ce include cea mai veche parte a oraşului, pot fi evidenţiate cîteva zone caracteristice. Acestea sunt, în primul rînd, zona Puşkin, în limitele ei stabilite. Aici s-a păstrat planificarea neregulată veche şi un şir de monumente arhitecturale şi istorice. Locul central îl ocupă construcţiile şi spaţiile legate de aflarea lui A.S.Puşkin în Chişinău. A doua zonă se află între str.Puşkin şi str.Ismail, unde s-a păstrat planificarea veche, un şir de clădiri istorice semnificative, inclusiv biserica Mazarachi - cea mai veche clădire din oraş. Imagine caracteristică o posedă partea centrului între aceste 2 zone, formată după proiectul lui Şciusev şi construită în anii 1950-1980 .

O parte importantă a sectorului istoric o reprezintă zona ce se află pe malul stîng al r.Bîc, ce include colina pe care s-au păstrat rămăşiţele cetăţii terestre şi este amplasată biserica sf. Constantin şi Elena şi terenul văii aderent.

Relativ independentă şi deosebită de alte părţi ale oraşului este zona de la sud de colina centrală limitată de strazile Ismail şi Albişoara, bd. Ştefan cel Mare şi bd. C.Negruzzi. Imaginea ei este determinată de o construcţie de proporţii mari.

Din sud, la colina centrală aderă încă 2 zone caracteristice. Una s-a format de-a lungul bd.Iu. Gagarin, alta ocupă o parte din colină de-a lungul str.Malina Mare.

Valoarea mediului în limitele diferitor zone este diferită. Pe lîngă construcţii importante ele conţin un şir de obiecte în disonanţă cu ambianţa.

Principala axă compoziţională de planificare a centrului este bd.Ştefan cel Mare. Tot cu el sunt legate principalele spaţii intraurbane centrale. Ele aderă nemijlocit la bulevard, amplifică formarea acestei străzi, celei mai largi încă pe primele planuri de la înc. sec XIX. De-a lungul ei sunt amplasate cele mai reprezentative clădiri ale oraşului.

O axă compoziţională importantă a centrului este bd.Gr.Vieru (fosta Renaşterii). Ea este legată organic cu planificarea regulată a centrului şi cu centrul compoziţional istoric al acestei părţi a oraşului. Bulevardul leagă centrul cu spaţiul văii r.Bîc şi continuă pe malul stîng al acestuia. Din partea văii  bulevardul este accentuat activ de clădiri înalte.

Spaţii intraurbane considerabile se formează de asemenea de-a lungul bulevardelor Negruzzi şi Gagarin, reprezentând o continuare a axei compoziţionale principale de planificare a centrului.

Rol important în compoziţia centrului îl joacă dominantele arhitecturale istorice şi contemporane. Dominantele istorice sunt mici ca înălţime şi dimensiuni. Însă, ocupînd loc convenabil pentru observare, ele joacă un rol important în compoziţia şi imaginea oraşului. În primul rînd aceasta se referă la biserica sf.Constantin şi Elena, biserica Mazarachi şi Catedrala. Obiectele noi semnificative sunt amplasate în aşa fel ca să păstreze rolul compoziţional activ al  dominantelor istorice. Iar cele trei case înalte amplasate lîngă biserica Mazarachi, creează împreună cu ea un complex arhitectural memorabil şi accentuează colina-cheie în structura spaţială a centrului, legată strîns de apariţia oraşului.

Noile dominante arhitecturale sunt plasate de-a lungul axei compoziţionale principale, diversificînd spaţiul intraurban de bază, şi de-a lungul frontului construcţiilor ce iese spre valea rîului Bîc, care defineşte imaginea centrului la observarea acestuia din spaţii exterioare. Însă acest front este relativ închis. Clădirile înalte delimitează valea de sectoarele centrului de pe cealaltă parte a ei, iar centrul - de sectoarele exterioare.

< Înapoi la [Cuprins](../../cuprins.md) | [Capitolul III. Analiza situației existente](../v1_cap3.md)