< Înapoi la [Cuprins](../cuprins.md)

## Capitolul III. Analiza situaţiei existente ##
* [3.1 Condițiile naturale](v1_cap3/v1_cap3.1.md)
  * 3.1.1 Condițiile geomorfologice
  * 3.1.2 Condițiile geologico-inginerești
  * 3.1.3 Condițiile hidrogeologice
  * 3.1.4 Condițiile ecologice 
* [3.2 Evaluarea arhitectural-peisajistică](v1_cap3/v1_cap3.2.md)
* [3.3 Situația urbanistică și spațial-volumetrică](v1_cap3/v1_cap3.3.md)
* [3.4 Descriere patrimonială](v1_cap3/v1_cap3.4.md)
* [3.5 Zonificare funcțională](v1_cap3/v1_cap3.5.md)
* [3.6 Obiective social-culturale, administrative, științifice, de educație, învățământ, sportive și de producere](v1_cap3/v1_cap3.6.md)
* [3.7 Situația socio-economică](v1_cap3/v1_cap3.7.md)
* [3.8 Populația, situația demografică](v1_cap3/v1_cap3.8.md)
* [3.9 Spațiul locativ](v1_cap3/v1_cap3.9.md)
* [3.10 Structura stradală și de transport](v1_cap3/v1_cap3.10.md)

< Înapoi la [Cuprins](../cuprins.md)
